# Hivemind
Hivemind is a application for the tabletop game Necromunda by Games Workshop.

Hivemind keeps track of your gang and performs updates for their injuries, experience and levels. I'm planning on adding additional features including a storefront.

My brother and I recently started playing Necromunda again for the first time since we were kids. We were finding that a lot of time was spent every time we'd get together to play going through the post-game activities. I decided to make an application that takes care of all of this so we could get back to cyberpunk battles in the grimdark future.

Initially I was using the Excel sheets that we had been using for our Gang information as a source, but the issue was that the sheet needed to be formatted just so in order to function properly. Instead I set up a mySQL database on a computer running a tomcat server that contains all of the information. I've posted a database dump so anyone who wants to use Hivemind for their own games.

## Setting it up on a Raspberry Pi
If you want to set Hivemind up to run on a Raspberry Pi (or more likely, I've broken something and forgotten how I got it running in the first place) here's how to do it. You need Java 8, Tomcat 8 and MySQL. I'm not sure on the version for MySQL, but I've got 5.5 on the Pi right now and it's working fine.

Clone this repository:

```
$ git clone https://github.com/weirdvector/Hivemind.git
```

I'm not exactly sure which repository you need for MySQL. Currently these are what I've got installed on the Pi:
mysql-client, mysql-client-5.5, mysql-common, mysql-server, mysql-server-5.5, mysql-server-core-5.5

Install those, along with Java 8.

```
$ sudo apt-get install oracle-java8-jdk
```

Tomcat 8 wasn't in the Raspbian repositories yet, so get it from the Apache site.

```
$ wget http://apache.mirror.iweb.ca/tomcat/tomcat-8/v8.0.23/bin/apache-tomcat-8.0.23.tar.gz
$ tar xvzf aparche-tomcat-8.0.23.tar.gz
$ mv apache-tomcat-8.0.23 /opt/tomcat
```

Edit the port numbers.

```
$ vi /opt/tomcat/apache-tomcat-8.0.23/conf/server.xml
```

Edit a <Connector> tag to point to whatever port you like.

```
<Connector port="8081" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443"/>
```

Add profiles:

```
$ vi /opt/tomcat/apache-tomcat-8.0.23/conf/tomcat-users.xml
```

Add the following lines between your <tomcat-users> tags:

```
<role rolename="tomcat"/>
<role rolename="manager"/>
<role rolename="manager-gui"/>
<user username="tomcat" password="tomcat" roles="tomcat"/>
<user username="hivemind" password="necromunda" roles="manager,tomcat,manager-gui"/>
```

Edit your .bashrc.

```
$ vi .bashrc
export CATALINA_HOME="/opt/tomcat/apache-tomcat-8.0.23"
export JAVA_HOME="/usr/lib/jvm/jdk-8-oracle-arm-vfp-hflt"
```

Commit changes to your .bashrc .

```
$ . ~/.bashrc
```

Copy the .WAR file into your Tomcat /webapps directory.

```
$ sudo cp Hivemind/dist/Hivemind.war /opt/tomcat/apache-tomcat-8.0.23/webapps/
```

Once those are installed, start MySQL and add the Hivemind user.

```mysql
$ mysql -u root -p
mysql> CREATE USER 'hivemind'@'localhost' IDENTIFIED BY 'necromunda';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'hivemind'@'localhost';
```

Then make your database and populate it with the SQL file.

```mysql
mysql> CREATE DATABASE hivemind;
mysql> USE hivemind;
mysql> SOURCE hivemind.sql;
```

Now you're good to go! Start up your Tomcat server, and the WAR file should deploy.

```
$ $CATALINA_HOME/bin/startup.sh
```

Open a web browser and go to (Raspberry Pi's IP):8081/Hivemind

Games Workshop owns Necromunda. No copyright infringement is intended.
