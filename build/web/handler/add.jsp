<%-- 
    Document   : add
    Created on : 28-Mar-2015, 2:55:36 PM
    Author     : weirdvector
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    Ganger g = new Ganger();
    g.setName(request.getParameter("gangerName"));
    g.setCost(Integer.parseInt(request.getParameter("cost")));
    g.setExp(Integer.parseInt(request.getParameter("experience")));
    g.setMove(Integer.parseInt(request.getParameter("move")));
    g.setWs(Integer.parseInt(request.getParameter("ws")));
    g.setBs(Integer.parseInt(request.getParameter("bs")));
    g.setStrength(Integer.parseInt(request.getParameter("s")));
    g.setToughness(Integer.parseInt(request.getParameter("t")));
    g.setWounds(Integer.parseInt(request.getParameter("w")));
    g.setInitiative(Integer.parseInt(request.getParameter("i")));
    g.setAttack(Integer.parseInt(request.getParameter("a")));
    g.setLeadership(Integer.parseInt(request.getParameter("ld")));
    g.setSkills(request.getParameter("skills"));
    g.setInjuries(request.getParameter("injuries"));
    g.setGangId(Integer.parseInt(request.getParameter("gangId")));
    g.setGangerType(request.getParameter("type").charAt(0));
    
    gangBean.addGanger(g);
    
    //if stash items are selected, add them
    if (request.getParameterValues("stash") != null) {
        String[] equipment = request.getParameterValues("stash");
        for (String e : equipment) {
            String[] idPrice = e.split("cost");
            int weaponId = Integer.parseInt(idPrice[0]);
            int cost = Integer.parseInt(idPrice[1]);
            gangBean.setEquipment(weaponId, g, cost);
            gangBean.removeFromStash(g.getGangId(), weaponId);
        }
    }
    
    response.sendRedirect("../index.jsp?id=" + g.getGangId());
%>
