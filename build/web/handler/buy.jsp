<%-- 
    Document   : buy
    Created on : 1-Apr-2015, 8:47:59 PM
    Author     : weirdvector
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>


<%
    /*
     * Get the amount + Id of the weapons purchased, and add them to the 
     * stash for the gang id specified. (It's a session variable, check
     * index.jsp)
    */
    
    int gangId = Integer.parseInt(session.getAttribute("gangId").toString());
    
    //78 is the number of equipment items
    for (int id = 0; id < 78; id++) {
        String amount = "amount" + id;
        if (request.getParameter(amount) != null) {
            //buy the number of the item requested.
            int number = Integer.parseInt(request.getParameter(amount));
            int cost = Integer.parseInt(request.getParameter("costActual" + id));
            if (number > 0) {
                for (int j = 0; j < number; j++) {
                    gangBean.addWeaponToStash(gangId, id, cost);
                }
            }
        }
    }
    
    //Subtract the name "total" value from the gang's credits.
    //Maybe this should be changed to recalculate the total price, to prevent 
    //messing with the form info
    int total = Integer.parseInt(request.getParameter("total"));
    total *= -1;
    gangBean.increaseCredits(total, gangId);
    
    //redirect back to the index page
    response.sendRedirect("../index.jsp?id=" + session.getAttribute("gangId"));
    
%>

