<%-- 
    Document   : updateGanger
    Created on : 28-Mar-2015, 2:23:53 PM
    Author     : weirdvector
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    Ganger g = new Ganger();
    g.setName(request.getParameter("gangerName"));
    g.setExp(Integer.parseInt(request.getParameter("experience")));
    g.setGangerId(Integer.parseInt(request.getParameter("gangerId")));
    g.setMove(Integer.parseInt(request.getParameter("move")));
    g.setWs(Integer.parseInt(request.getParameter("ws")));
    g.setBs(Integer.parseInt(request.getParameter("bs")));
    g.setStrength(Integer.parseInt(request.getParameter("s")));
    g.setToughness(Integer.parseInt(request.getParameter("t")));
    g.setWounds(Integer.parseInt(request.getParameter("w")));
    g.setInitiative(Integer.parseInt(request.getParameter("i")));
    g.setAttack(Integer.parseInt(request.getParameter("a")));
    g.setLeadership(Integer.parseInt(request.getParameter("ld")));
    //g.setSkills(request.getParameter("skills"));
    //g.setInjuries(request.getParameter("injuries"));
    g.setGangId(Integer.parseInt(request.getParameter("gangId")));
    g.setGangerType(request.getParameter("type").charAt(0));
    
    //equipment
    if (request.getParameterValues("stash") != null) {
        String[] equipment = request.getParameterValues("stash");
        for (String e : equipment) {
            String[] idPrice = e.split("cost");
            int weaponId = Integer.parseInt(idPrice[0]);
            int cost = Integer.parseInt(idPrice[1]);
            gangBean.setEquipment(weaponId, g, cost);
            gangBean.removeFromStash(g.getGangId(), weaponId);
        }
    }
    
    //injuries
    //if the injuries list is blank, it returns null. Remove all injuries before continuing.
    //It's not a problem, existing injuries are autoselected so should not be removed.
    gangBean.removeAllInjuries(g.getGangerId());
    if (request.getParameterValues("injuries") != null) {
        //now add all the injuries passed through as parameters
        String[] injuries = request.getParameterValues("injuries");
        for (String i: injuries) {
            gangBean.addInjury(g.getGangerId(), Integer.parseInt(i));
        }
    }
    
    //Skills
    //if the skills list is blank, it returns null. Remove all skills before continuing.
    //It's not a problem, existing skills are autoselected so should not be removed.
    gangBean.removeAllSkills(g.getGangerId());
    if (request.getParameterValues("skills") != null) {
        //now add all the injuries passed through as parameters
        String[] skills = request.getParameterValues("skills");
        for (String s: skills) {
            gangBean.addSkill(g.getGangerId(), Integer.parseInt(s));
        }
    }
    
    //set cost last
    g.setCost(gangBean.calcGangerCost(g));
    
    gangBean.updateGanger(g);
    
    response.sendRedirect("../index.jsp?id=" + request.getParameter("gangId"));
%>