var c = document.getElementById("c");
var ctx = c.getContext("2d");
var w = c.width;
var h = c.height;

var grd=ctx.createLinearGradient(0,0,0,h);
grd.addColorStop(0,"#559955");
grd.addColorStop(0.5,"#55FF55");
grd.addColorStop(1,"#559955");

function drawBar(xp) {
  ctx.fillColor = "black";
  ctx.fillRect(0,0,w,h);
  
  var next = nextLevel(xp);
  var prev = previousLevel(xp);
  
  var xpDisp = xp - prev;
  var nextDisp = next - prev;
  
  ctx.fillStyle = grd;
  ctx.fillRect(0,0,xpDisp/nextDisp*w,h);  
  
  ctx.font = "15px arial";  
  ctx.fillStyle = "white";
  ctx.fillText( prev + "xp", 5, h-5);
  ctx.fillText( next + "xp", w-50, h-5);
}

function nextLevel(xp) {
  if (xp < 5) {
    return 5;
  } else if (xp < 10) {
    return 10;
  } else if (xp < 15) {
    return 15;
  } else if (xp < 20) {
    return 20;
  } else if (xp < 30) {
    return 30;
  } else if (xp < 40) {
    return 40;
  } else if (xp < 50) {
    return 50;
  } else if (xp < 60) {
    return 60;
  } else if (xp < 80) {
    return 80;
  } else if (xp < 100) {
    return 100;
  } else if (xp < 120) {
    return 120;
  } else if (xp < 140) {
    return 140;
  } else if (xp < 160) {
    return 160;
  } else if (xp < 180) {
    return 180;
  } else if (xp < 200) {
    return 200;
  } else if (xp < 240) {
    return 240;
  } else if (xp < 280) {
    return 280;
  } else if (xp < 320) {
    return 320;
  } else if (xp < 360) {
    return 360;
  } else if (xp < 400) {
    return 400;
  } else {
    return 3000;
  }
}

function previousLevel(xp) {
  if (xp < 5) {
    return 0;
  } else if (xp < 10) {
    return 5;
  } else if (xp < 15) {
    return 10;
  } else if (xp < 20) {
    return 15;
  } else if (xp < 30) {
    return 20;
  } else if (xp < 40) {
    return 30;
  } else if (xp < 50) {
    return 40;
  } else if (xp < 60) {
    return 50;
  } else if (xp < 80) {
    return 60;
  } else if (xp < 100) {
    return 80;
  } else if (xp < 120) {
    return 100;
  } else if (xp < 140) {
    return 120;
  } else if (xp < 160) {
    return 140;
  } else if (xp < 180) {
    return 160;
  } else if (xp < 200) {
    return 180;
  } else if (xp < 240) {
    return 200;
  } else if (xp < 280) {
    return 240;
  } else if (xp < 320) {
    return 280;
  } else if (xp < 360) {
    return 320;
  } else if (xp < 400) {
    return 360;
  } else {
    return 400;
  }
}