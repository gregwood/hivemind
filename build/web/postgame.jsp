<%-- 
    Document   : postgame
    Created on : 28-Mar-2015, 6:41:05 PM
    Author     : weirdvector
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>

    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel='stylesheet' type='text/css' href='css/hivemind.css'>
            <link rel="shortcut icon" href="img/icon.png" />
            <title>Post Game Report</title>
        </head>
        <body>
            <div id='container'>
                <h1>Post Game Report</h1> 
                <div id='secondPanel'>
                    <h2>Injuries</h2>
                <%
                    Gang gang = (Gang) session.getAttribute("gang");
                    ArrayList<Ganger> ganglist = gang.getGangArrayList();

                    //make the new Gang to put the updated Gangers into
                    Gang updatedGang = new Gang();
                    updatedGang.setGangName(gang.getGangName());
                    updatedGang.setGangHouse(gang.getGangHouse());
                    updatedGang.setGangId((Integer) session.getAttribute("gangId"));
                    updatedGang.setCredits(gang.getCredits());

                    Injuries inj = new Injuries();
                    Experience exp = new Experience();

                    //determine injuries
                    for (Ganger g : ganglist) {

                        //make sure the ganger is active first.
                        if (request.getParameter("active" + g.getGangerId()) != null) {

                            //see if the downed fighters need to roll on the injury table
                            if (request.getParameter("down" + g.getGangerId()) != null) {
                                if (Math.random() > .5) {
                                    g = inj.findInjury(g, gang.getGangHouse());
                                    out.print("<div>");
                                    out.print(inj.getReport());
                                    out.print("</div>");
                                }
                            }

                            if (request.getParameter("outOfAction" + g.getGangerId()) != null) {
                                g = inj.findInjury(g, gang.getGangHouse());
                                out.print("<div>");
                                out.print(inj.getReport());
                                out.print("</div>");
                            }
                        }
                    }

                    //Calculate Experience
                %>
                <h2>Experience</h2>
                <form action='skills.jsp' method='post'>
                    <%                        
                        String gameType = request.getParameter("gameType");
                        boolean hasWon = (request.getParameter("winLose").equals("win")) ? true : false;
                        boolean obj1, obj2, obj3;

                        for (Ganger g : ganglist) {

                            //make sure the ganger is active first.
                            if (request.getParameter("active" + g.getGangerId()) != null) {

                                //check the objectives checkboxes
                                boolean objs[] = new boolean[3];
                                if (request.getParameter("obj" + g.getGangerId()) != null) {
                                    String[] objectives = (request.getParameter("obj" + g.getGangerId())).split(",");
                                    for (int i = 0; i < objectives.length; i++) {
                                        objs[i] = true;
                                    }
                                }

                                //check the kills checkboxes
                                boolean kills[] = new boolean[3];
                                if (request.getParameter("kills" + g.getGangerId()) != null) {
                                    String[] killsCheck = (request.getParameter("kills" + g.getGangerId())).split(",");
                                    for (int i = 0; i < killsCheck.length; i++) {
                                        kills[i] = true;
                                    }
                                }

                                boolean isAttacker = (request.getParameter("attackDefend").equals("attack")) ? true : false;
                                int oppGangRating = Integer.parseInt(request.getParameter("oppGangRating"));

                                g = exp.calcExperience(g, gameType, hasWon, objs[0], objs[1], objs[2], kills[0], kills[1], kills[2], isAttacker, oppGangRating, gang.getGangRating(), gang.getGangHouse());

                                if (exp.getReport() != null) {
                                    out.print(exp.getReport());
                                }
                            }

                            updatedGang.addGanger(g);
                        }

                        //set the gang as an attribute so it can be used on the next page
                        session.setAttribute("gang", updatedGang);
                    %>

                    <h2>Income</h2>
                    <%
                        //get the arraylist of territories from the ganglist, use that to produce income report.
                        Income income = new Income();
                        ArrayList<Territory> territories = gangBean.getGangTerritory(gang.getGangId());
                        out.print(income.getReport(gang, territories));
                    %>
                    <input type='submit'>
                </form>
            </div>
        </div>
    </body>
</html>
