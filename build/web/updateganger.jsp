<%--    
    Document   : adminEditGanger
    Created on : 28-Mar-2015, 12:59:20 PM
    Author     : weirdvector
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    String id = request.getParameter("id");
    Ganger g = gangBean.getGanger(id);
    ArrayList<Gang> ganglist = gangBean.getGangs();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hivemind</title>
        <link rel="stylesheet" type="text/css" href="css/hivemind.css">
        <link rel="shortcut icon" href="img/icon.png" />        
    </head>
    <body onload="drawBar(<%= g.getExp()%>)">
        <div id="container">
            <h1>Admin mode</h1>
            <div id="editPanel">
                <form action="handler/update.jsp" method="post">
                    <table id="editGanger">
                        <tr>
                            <th colspan='10'>Edit Ganger</th>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td colspan="2"><input type="text" value="<%= g.getName()%>"name="gangerName"></td>
                            <td>
                                <select name="type">
                                    <option value="J" <% if (g.getGangerType() == 'J') {
                                            out.print("selected='selected'");
                                        }%>>J</option>
                                    <option value="G" <% if (g.getGangerType() == 'G') {
                                            out.print("selected='selected'");
                                        }%>>G</option>
                                    <option value="H" <% if (g.getGangerType() == 'H') {
                                            out.print("selected='selected'");
                                        }%>>H</option>
                                    <option value="L" <% if (g.getGangerType() == 'L') {
                                            out.print("selected='selected'");
                                        }%>>L</option>
                                </select>
                            </td>
                            <td>Cost:</td>
                            <td><input type="text" value="<%= gangBean.calcGangerCost(g)%>" size="2" name="cost" readonly="readonly"></td>
                            <td>Exp:</td>
                            <td><input type="text" value="<%= g.getExp()%>" size="2" name="experience"></td>
                            <td>ID:</td>
                            <td><input type="text" value="<%= g.getGangerId()%>" size="2" name="gangerId" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>M</td>
                            <td>WS</td>
                            <td>BS</td>
                            <td>S</td>
                            <td>T</td>
                            <td>W</td>
                            <td>I</td>
                            <td>A</td>
                            <td>Ld</td>
                        </tr>
                        <tr>
                            <td>Stats:</td>
                            <td><input type="text" value="<%= g.getMove()%>" size="2" maxlength="1" name="move"></td>
                            <td><input type="text" value="<%= g.getWs()%>" size="2" maxlength="1" name="ws"></td>
                            <td><input type="text" value="<%= g.getBs()%>" size="2" maxlength="1" name="bs"></td>
                            <td><input type="text" value="<%= g.getStrength()%>" size="2" maxlength="1" name="s"></td>
                            <td><input type="text" value="<%= g.getToughness()%>" size="2" maxlength="1" name="t"></td>
                            <td><input type="text" value="<%= g.getWounds()%>" size="2" maxlength="1" name="w"></td>
                            <td><input type="text" value="<%= g.getInitiative()%>" size="2" maxlength="1" name="i"></td>
                            <td><input type="text" value="<%= g.getAttack()%>" size="2" maxlength="1" name="a"></td>
                            <td><input type="text" value="<%= g.getLeadership()%>" size="2" maxlength="1" name="ld"></td>
                        </tr>
                        <tr>
                            <td>Skills:</td>
                            <td colspan="4">
                                <select name="skills" size="5" multiple>
                                    <%
                                        out.print(gangBean.getGangerSkills(g.getGangerId()));
                                        out.print(gangBean.getOtherSkills(g.getGangerId()));
                                    %>
                                </select>
                            </td>
                            <td>Injuries:</td>
                            <td colspan="4">
                                <select name='injuries' size='5' multiple>
                                    <%
                                        out.print(gangBean.getGangerInjuries(g.getGangerId()));
                                        out.print(gangBean.getOtherInjuries(g.getGangerId()));
                                    %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Equipment:</td>
                            <td colspan="9">
                                <%
                                    ArrayList<Weapon> equipment = gangBean.getGangerEquipment(g.getGangerId());
                                    for (int i = 0; i < equipment.size(); i++) {
                                        out.print("<div class='equippedWeapon'>" + equipment.get(i).getWeaponName() + ""
                                                + "<label class='removeWeapon'>"
                                                + "<a href='handler/removeweapon.jsp?id=" + g.getGangerId() + "&wepId=" + equipment.get(i).getWeaponId() + "&gangId=" + g.getGangId() + "&cost=" + equipment.get(i).getCost() + "'>"
                                                + "<img src='img/remove.png'/>"
                                                + "</a>"
                                                + "</label>"
                                                + "</div>");
                                    }
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                <canvas id='c' height='20' width='625'></canvas>
                            </td>
                        </tr>
                        <tr>
                            <td>Gang</td>
                            <td colspan='3'>
                                <select name="gangId">
                                    <% for (int i = 0; i < ganglist.size(); i++) {
                                            if (ganglist.get(i).getGangId() == g.getGangId()) {
                                                out.print("<option selected='selected' value='"
                                                        + +ganglist.get(i).getGangId() + "'>"
                                                        + ganglist.get(i).getGangName() + "</option>");
                                            } else {
                                                out.print("<option value='" + ganglist.get(i).getGangId()
                                                        + "'>" + ganglist.get(i).getGangName()
                                                        + "</option>");
                                            }
                                        }
                                    %>
                                </select>
                            </td>
                            <td colspan="6" class='buttonCell'>
                                <input type='submit' value="Update">
                            </td>
                        </tr>
                    </table>
                    <table id="weaponList">
                        <tr>
                            <th>Stash</th>
                        </tr>
                        <tr>
                            <td>
                                <select id="equipment" name="stash" size="10" multiple>
                                    <%
                                        ArrayList<Weapon> weapons = gangBean.getStash(g.getGangId());
                                        for (Weapon weapon : weapons) {
                                            out.print("<option value='" + weapon.getWeaponId() + "cost" + weapon.getCost() + "'>" + weapon.getWeaponName() + "</option>");
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="js/xp.js"></script>
    </body>
</html>
