-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (armv7l)
--
-- Host: localhost    Database: hivemind
-- ------------------------------------------------------
-- Server version	5.5.43-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `equipmentId` int(11) NOT NULL AUTO_INCREMENT,
  `weaponId` int(11) NOT NULL,
  `gangerId` int(11) NOT NULL,
  `cost` int(11) DEFAULT NULL,
  PRIMARY KEY (`equipmentId`),
  KEY `weaponId` (`weaponId`),
  KEY `gangerId` (`gangerId`),
  CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`weaponId`) REFERENCES `weapons` (`weaponId`),
  CONSTRAINT `equipment_ibfk_2` FOREIGN KEY (`gangerId`) REFERENCES `gangers` (`gangerId`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (63,4,11,25),(64,16,11,25),(65,12,14,20),(66,38,14,120),(67,23,15,20),(68,43,15,30),(69,22,16,25),(70,43,16,30),(71,20,17,20),(72,43,17,30),(73,22,18,25),(74,13,19,15),(75,11,20,15),(76,17,20,10),(77,11,21,15),(79,13,22,15),(80,22,1,25),(81,43,1,30),(82,13,2,15),(84,22,3,25),(85,43,3,30),(86,22,4,25),(87,22,5,25),(88,22,6,25),(89,22,7,25),(90,22,8,25),(91,43,8,30),(95,48,1,20),(96,48,26,20),(97,50,29,15),(98,21,25,35),(99,43,25,30),(100,13,26,15),(101,35,26,180),(102,21,27,35),(103,43,27,30),(104,23,28,20),(105,24,28,20),(106,25,28,5),(107,42,28,50),(108,13,30,15),(109,12,31,20),(110,13,31,15),(111,22,32,25),(112,22,29,25),(113,43,29,30),(114,21,22,35),(115,53,25,45),(116,69,1,20),(117,29,2,130),(118,42,2,50),(119,22,9,25),(120,43,9,30),(121,13,33,15);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gangerInjuries`
--

DROP TABLE IF EXISTS `gangerInjuries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gangerInjuries` (
  `gangerInjuryId` int(11) NOT NULL AUTO_INCREMENT,
  `gangerId` int(11) NOT NULL,
  `injuryId` int(11) NOT NULL,
  PRIMARY KEY (`gangerInjuryId`),
  KEY `gangerId` (`gangerId`),
  KEY `injuryId` (`injuryId`),
  CONSTRAINT `gangerInjuries_ibfk_1` FOREIGN KEY (`gangerId`) REFERENCES `gangers` (`gangerId`),
  CONSTRAINT `gangerInjuries_ibfk_2` FOREIGN KEY (`injuryId`) REFERENCES `injuries` (`injuryId`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gangerInjuries`
--

LOCK TABLES `gangerInjuries` WRITE;
/*!40000 ALTER TABLE `gangerInjuries` DISABLE KEYS */;
INSERT INTO `gangerInjuries` VALUES (10,3,10),(15,7,11),(16,7,14),(17,4,9),(18,6,2),(23,14,7),(24,14,10),(25,15,6),(26,18,6),(27,21,11),(29,1,5),(30,17,11),(31,18,6),(33,30,4),(34,32,2),(35,5,1),(37,31,7),(38,4,5);
/*!40000 ALTER TABLE `gangerInjuries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gangerSkills`
--

DROP TABLE IF EXISTS `gangerSkills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gangerSkills` (
  `gangerSkillId` int(11) NOT NULL AUTO_INCREMENT,
  `gangerId` int(11) NOT NULL,
  `skillId` int(11) NOT NULL,
  PRIMARY KEY (`gangerSkillId`),
  KEY `gangerId` (`gangerId`),
  KEY `skillId` (`skillId`),
  CONSTRAINT `gangerSkills_ibfk_1` FOREIGN KEY (`gangerId`) REFERENCES `gangers` (`gangerId`),
  CONSTRAINT `gangerSkills_ibfk_2` FOREIGN KEY (`skillId`) REFERENCES `skills` (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gangerSkills`
--

LOCK TABLES `gangerSkills` WRITE;
/*!40000 ALTER TABLE `gangerSkills` DISABLE KEYS */;
INSERT INTO `gangerSkills` VALUES (6,4,7),(7,4,24),(8,6,36),(21,11,12),(22,11,19),(23,15,33),(24,17,11),(25,19,34),(26,19,35),(28,1,19),(31,27,18),(32,29,17),(33,29,31),(34,30,17),(35,30,35),(36,2,19),(37,2,32),(38,2,34),(39,30,2),(40,31,36),(41,1,30),(45,9,1),(46,9,19),(47,9,22),(48,32,3),(49,2,33);
/*!40000 ALTER TABLE `gangerSkills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gangers`
--

DROP TABLE IF EXISTS `gangers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gangers` (
  `gangerId` int(11) NOT NULL AUTO_INCREMENT,
  `gangId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(1) NOT NULL,
  `move` int(11) DEFAULT '4',
  `ws` int(11) DEFAULT '3',
  `bs` int(11) DEFAULT '3',
  `s` int(11) DEFAULT '3',
  `t` int(11) DEFAULT '3',
  `w` int(11) DEFAULT '1',
  `i` int(11) DEFAULT '4',
  `a` int(11) DEFAULT '1',
  `ld` int(11) DEFAULT '7',
  `skills` varchar(2000) DEFAULT '',
  `injuries` varchar(2000) DEFAULT '',
  `cost` int(11) DEFAULT NULL,
  `experience` int(11) DEFAULT NULL,
  `dead` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gangerId`),
  KEY `gangId` (`gangId`),
  CONSTRAINT `gangers_ibfk_1` FOREIGN KEY (`gangId`) REFERENCES `gangs` (`gangId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gangers`
--

LOCK TABLES `gangers` WRITE;
/*!40000 ALTER TABLE `gangers` DISABLE KEYS */;
INSERT INTO `gangers` VALUES (1,1,'Tsygan (1)','L',4,4,4,3,3,1,3,1,8,'null True Grit.','null',215,151,0),(2,1,'ZIB (2)','H',4,3,4,3,3,1,3,1,7,'null Gunfighter.','null',255,169,0),(3,1,'Dezik (3)','G',4,3,3,3,4,1,3,3,7,'','null',105,106,0),(4,1,'Laika (4)','G',4,2,4,4,3,1,3,1,7,'null','nullHead Wound, ',75,70,0),(5,1,'Veterok (5)','G',4,3,3,3,3,1,4,1,7,'','',75,39,1),(6,1,'Belka (6)','G',4,4,4,3,3,1,3,1,7,'null','null',75,87,0),(7,1,'Mushka (7)','G',4,3,4,4,3,1,3,1,8,'Hatred, ','null',75,112,0),(8,1,'Chernuska (8)','G',4,3,4,4,3,1,3,1,8,'','',105,99,0),(9,1,'Ugolyok (9)','G',4,3,2,3,4,1,4,1,7,'null','null',105,76,0),(10,2,'Sexton','L',4,3,3,3,3,1,4,1,7,'True Grit, Crack Shot, ','',170,102,1),(11,3,'Abraxas','L',4,4,4,3,3,2,4,1,8,'null','null',170,137,0),(14,3,'Orro','H',4,4,3,3,4,1,3,2,6,'null','null',200,120,0),(15,3,'Stux','G',4,3,3,3,3,1,4,1,8,'null','null',100,55,0),(16,3,'Half','G',4,5,3,3,4,1,4,1,8,'null','null',105,76,0),(17,3,'Brain','G',4,3,3,3,3,2,3,1,7,'null','null',100,65,0),(18,3,'Heartbleed','G',4,4,3,3,3,1,3,1,8,'null','null',75,52,0),(19,3,'Creep','G',4,2,2,4,3,1,2,1,7,'null','null',65,25,0),(20,3,'Koko','G',4,3,3,4,3,1,3,2,6,'null','null',75,38,0),(21,3,'Bonzi','G',4,2,3,3,3,1,3,2,7,'null','null',65,17,0),(22,3,'Wintermute','G',4,3,4,3,3,1,3,1,7,'null','null',100,37,0),(23,2,'Mason','H',4,4,3,3,3,1,4,1,6,'','',240,86,1),(24,2,'Cooper','H',4,4,3,3,3,2,3,1,7,'','',85,71,1),(25,2,'Collier, 9','L',4,3,3,3,3,1,5,1,8,'null','null',230,118,0),(26,2,'Lector 2','H',4,3,3,3,3,1,3,1,7,'null','null',275,92,0),(27,2,'Smith, 12','G',4,3,3,3,4,2,4,1,7,'null','null',115,141,0),(28,2,'Verger, 6','G',4,4,4,3,4,1,3,1,7,'null','null',145,126,0),(29,2,'Sawyer, 4','G',4,3,2,3,4,1,3,1,8,'null','null',120,64,0),(30,2,'Glazier, 8','G',4,3,2,2,4,1,3,1,6,'null Dodge.','null',65,38,0),(31,2,'Wharfie, 7','G',4,2,4,3,3,1,4,1,6,'null Rapid Fire.','null',60,30,0),(32,2,'Leecher,5','G',4,3,3,3,2,1,3,1,8,'null Jump Back.','null',75,49,0),(33,1,'Zvyozdochka (5)','J',4,3,2,2,3,1,3,1,6,'null','null',40,9,0);
/*!40000 ALTER TABLE `gangers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gangs`
--

DROP TABLE IF EXISTS `gangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gangs` (
  `gangId` int(11) NOT NULL AUTO_INCREMENT,
  `gangName` varchar(200) DEFAULT NULL,
  `house` varchar(50) DEFAULT NULL,
  `credits` int(11) DEFAULT '1000',
  PRIMARY KEY (`gangId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gangs`
--

LOCK TABLES `gangs` WRITE;
/*!40000 ALTER TABLE `gangs` DISABLE KEYS */;
INSERT INTO `gangs` VALUES (1,'Sobaki v Kosmose','Goliath',500),(2,'The Peons','Delaque',676),(3,'DinoSaars','Van Saar',950);
/*!40000 ALTER TABLE `gangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `income` (
  `incomeId` int(11) NOT NULL AUTO_INCREMENT,
  `territoryId` int(11) NOT NULL,
  `gangId` int(11) NOT NULL,
  PRIMARY KEY (`incomeId`),
  KEY `territoryId` (`territoryId`),
  KEY `gangId` (`gangId`),
  CONSTRAINT `income_ibfk_1` FOREIGN KEY (`territoryId`) REFERENCES `territories` (`territoryId`),
  CONSTRAINT `income_ibfk_2` FOREIGN KEY (`gangId`) REFERENCES `gangs` (`gangId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income`
--

LOCK TABLES `income` WRITE;
/*!40000 ALTER TABLE `income` DISABLE KEYS */;
INSERT INTO `income` VALUES (1,10,1),(2,3,1),(3,14,1),(4,9,1),(7,16,2),(8,2,2),(9,13,2),(10,8,3),(11,3,3),(12,16,3),(13,11,3),(14,5,3);
/*!40000 ALTER TABLE `income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `injuries`
--

DROP TABLE IF EXISTS `injuries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `injuries` (
  `injuryId` int(11) NOT NULL AUTO_INCREMENT,
  `injuryName` varchar(50) DEFAULT NULL,
  `injuryText` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`injuryId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `injuries`
--

LOCK TABLES `injuries` WRITE;
/*!40000 ALTER TABLE `injuries` DISABLE KEYS */;
INSERT INTO `injuries` VALUES (1,'Dead. ','The fighter is killed in action and his body abandoned\nto the mutant rats of the Underhive. All the weapons\nand equipment carried by the fighter are lost.\n'),(2,'Chest Wound. ','The fighter has been badly wounded in the chest. He\nrecovers but is weakened by the injury and his\nToughness characteristic is reduced by -1.\n'),(3,'Leg Wound. ','The fighter has smashed a leg. He recovers from his\ninjuries but he can no longer move quickly. The fighter’s\nMovement characteristic is reduced by -1. Randomly\ndetermine which leg has been hurt.\n'),(4,'Arm Wound. ','The fighter has smashed one arm. Although he recovers\nfrom his injury his strength is permanently reduced as a\nresult. The fighter’s Strength characteristic is reduced by\n-1 when using that arm. Randomly determine which\narm has been hit. Bear in mind that some hand-to-hand\nweapons use the fighter’s own Strength, eg swords.\n'),(5,'Head Wound. ','A serious head injury leaves the fighter somewhat\nunhinged. At the start of each game roll a D6 to\ndetermine how he is affected. On a 1-3 the fighter is\ndazed and confused – he is affected by the rules for\nstupidity. On a roll of 4-6 the fighter is enraged and\nuncontrollable – he is affected by the rules for frenzy.\n'),(6,'Blinded in one Eye. ','The fighter survives but loses the sight of one eye.\nRandomly determine which eye. A character with only\none eye has his Ballistic Skill reduced by -1. If the fighter\nis subsequently blinded in his remaining good eye then\nhe must retire from the gang.\n'),(7,'Partially Deafened. ','The fighter survives but is partially deafened as a result\nof his injuries. An individual suffers no penalty if he is\npartially deafened, but if he is deafened for a second\ntime he suffers -1 from his Leadership characteristic.\n'),(8,'Shell Shock. ','The fighter survives but is extremely nervous and jumpy\nas a result of the traumatic injuries he has suffered. His\nInitiative characteristic is reduced by -1.\n'),(9,'Hand Injury. ','Wounds to a hand result in the loss of D3 fingers.\nRandomly determine which hand is affected. The\nfighter’s Weapon Skill is reduced by -1. If a fighter loses\nall five fingers on a hand then he may no longer use that\nhand: he may not carry anything in it, and is unable to\nuse weapons that require two hands.\n'),(10,'Old Battle Wound. ','The fighter recovers but his old wound sometimes\naffects his health. Roll a D6 before each game. On the\nroll of a 1 the fighter’s old wound is playing up and he\nis unable to take part in the forthcoming battle.\n'),(11,'Bitter Enmity. ','Although he makes a full physical recovery, the fighter\nhas been psychologically scarred by his experiences. He\ndevelops a bitter enmity for the gang that was\nresponsible for his injury. From now on, the fighter\nhates\nthe gang responsible for his injury.'),(12,'Captured. ','The fighter is captured. Captives may be exchanged,\nransomed back or sold into slavery. If both gangs hold\ncaptives then they must be exchanged on a one-for-one\nbasis, starting with models of the highest value. Any\nremaining captives must be ransomed back to their own\ngang if the player is willing to pay the captor’s asking\nprice. There is no fixed value for ransom – it is a matter\nfor the players to decide for themselves. Finally, fighters\nwho are neither exchanged or ransomed may be sold to\nthe Guilders as slaves earning the captor D6 x 5 Guilder\ncredits. Captives who are exchanged or ransomed retain\nall of their weapons and equipment; if captives are sold\ntheir weaponry and equipment is kept by the captors.\n'),(13,'Horrible Scars. ','The fighter recovers from his injuries but is left horribly\ndisfigured. His scarred and distorted features inspire\nfear as described in the Advanced Rules section of the\nrulebook.\n'),(14,'Impressive Scars. ','The fighter recovers and is left with impressive scars as\ntestament to his bravery. Add +1 to the fighter’s\nLeadership characteristic. This bonus applies only once,\nfurther impressive scars have no additional effect.\n');
/*!40000 ALTER TABLE `injuries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills` (
  `skillId` int(11) NOT NULL AUTO_INCREMENT,
  `skillName` varchar(50) DEFAULT NULL,
  `skillText` varchar(2000) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,'Catfall. ','A model with the Catfall skill halves the distance fallen\nwhen calculating the strength of any hits which result\nfrom falling. Round fractions down.\n','Agility'),(2,'Dodge. ','A model with this skill receives a 6+ saving throw\nagainst hits from shooting or in hand-to-hand combat.\nThis is an unmodified save – ie, it is not affected by a\nweapon’s armour save modifier. The save is taken\nseparately and in addition to any saves for armour.\nIf a model successfully dodges from a weapon which\nuses a template or blast marker then move the model\nup to 2\n\". As long as his move gets him outside the\ntemplate area the fighter avoids the hit. Otherwise he\nmay still be hit, even though he has dodged\nsuccessfully.\n','Agility'),(3,'Jump Back. ','At the start of any hand-to-hand combat phase the\nmodel may attempt to disengage from combat by\njumping back. Roll a D6. If the score is less than the\nmodel’s Initiative it may immediately jump back 2\n\" leaving any hand-to-hand opponents behind. If the\nscore is equal to or greater than the model’s Initiative\nit must remain and continue to fight as normal.\n','Agility'),(4,'Leap. ','The model may leap D6 inches during the movement\nphase in addition to his normal movement. He may\nmove and leap, run and leap or charge and leap, but\nhe can only leap once during the turn.\n A leap will take the model over any man-high obstacle\nwithout penalty, including enemy models. In\naddition, the model can leap out of hand-to-hand\ncombat at the end of any hand-to-hand combat phase\nwithout suffering any penalty whatsoever.\nThe leap may be used to jump across gaps, but in this\ncase the player must commit the model to making the\nleap before rolling the dice to see how far the model\njumps. If the model fails to make it all the way across,\nthen it falls through the gap.\n','Agility'),(5,'Quick Draw. ','A model with this skill can double its Initiative when\nit makes a fast draw in a gunfight. See the Gunfight\nscenario to find how this works.\n','Agility'),(6,'Sprint. ','The model may triple its movement rate when it runs\nor charges, rather than doubling it as normal.\n','Agility'),(7,'Combat Master. ','If the model is attacked by multiple opponents in\nhand-to-hand combat then it can use the enemies’\nnumbers against them. For each opponent over one,\nadd +1 to the model’s Weapon Skill.\n','Combat'),(8,'Disarm. ','The model may use this skill against one close combat\nopponent at the start of the hand-to-hand combat\nphase. Roll a D6. On a roll of 4+ the enemy\nautomatically loses one weapon of your choice. This\nweapon is destroyed and can no longer be used – it is\ndeleted permanently from the gang roster. A model is\nalways assumed to have a knife, even if he has been\ndisarmed of all his other weapons.\n','Combat'),(9,'Feint. ','The model may ‘convert’ any parries it is allowed to\nuse into extra attacks at +1 A per parry. The attack is\nused instead of the parry. The model may choose to\nfeint or parry each time it attacks (eg, you could parry\none time and feint the next).\n','Combat'),(10,'Parry. ','A model with the Parry skill may parry in hand-to-\nhand combat even if he does not have a sword or\nanother weapon suitable for parrying. The model\nknocks aside blows using the flats of his hands or the\nhaft of his weapon. If the model has a weapon that\nmay parry, it may force an opponent to re-roll up to 2\nAttack dice when parrying, rather than just 1.\n','Combat'),(11,'Counter Attack. ','If a model carries a sword he is normally able to parry\n(force his opponent to re-roll his best Attack dice).\nHowever, a parry is cancelled out if the opponent is\nalso armed with a sword or has the Parry skill as\ndescribed above. If a fighter has the Counter Attack\nskill and his parry is cancelled for whatever reason,\nthen he may make a Counter Attack – roll an extra\nAttack dice immediately.\n','Combat'),(12,'Step Aside. ','The model has an uncanny ability to step aside and\ndodge blows in hand-to-hand combat. If the model is\nhit in hand-to-hand fighting roll a D6. On a roll of 4+\nthe model steps out of the way of the blow and is\nunharmed.\n','Combat'),(13,'Ambush. ','The model is allowed to go into overwatch and hide\nin the same turn. Normally a fighter must expend his\nentire turn to go into overwatch, but a fighter with the\nAmbush skill may do both.\n','Stealth'),(14,'Dive. ','A model with this skill can run and hide in the same\nturn. Normally a fighter who runs cannot hide in the\nsame turn, but a fighter with Dive skill can run and\nhide.\n','Stealth'),(15,'Escape Artist. ','This model may never be captured after a battle. If\nyou roll a ‘Captured’ result on the Serious Injuries\ntable then the fighter escapes unharmed together\nwith his equipment.\n','Stealth'),(16,'Evade. ','The model ducks and weaves as he moves making\nhim very hard to hit. Any enemy shooting from short\nrange suffers a -2 to hit penalty, while any enemy\nshooting at long range suffers a -1 penalty. This\npenalty only applies if the fighter is in the open and\nnot if he is behind cover.\n','Stealth'),(17,'Infiltration. ','A model with this skill is always placed on the\nbattlefield after the opposing gang and can be placed\nanywhere on the table as long as it is out of sight of\nthe opposing gang. If both players have models that\ncan infiltrate roll a D6 each, lowest roll sets up first.\n','Stealth'),(18,'Sneak up. ','Any sentry attempting to spot this model must halve\nhis normal spotting distance. Rules for sentries and\nspotting are covered in the relevant scenarios.\n','Stealth'),(19,'Body slam. ','The model adds +2 to its WS in the turn when it\ncharges instead of only +1.\n','Muscle'),(20,'Bulging Biceps. ','This skill may only be taken by a heavy. The heavy is\nallowed to move and shoot with weapons that would\nnormally restrict the model to either moving or\nshooting. However, if the model moves and shoots in\nthe same turn it suffers a -1 to hit penalty.\n','Muscle'),(21,'Crushing Blow. ','A model with Crushing Blow skill has a +1 Strength\ncharacteristic bonus in hand-to-hand combat. As a\nfighter’s own Strength is used as the basis for\ncalculating the strengths of hand-to-hand weapons\nthe bonus will apply to all such weapons.\n','Muscle'),(22,'Head Butt. ','If the model inflicts 2 or more hits in hand-to-hand\ncombat then he may choose to exchange all hits for a\nsingle hit with a further strength bonus. The bonus\nequals +1 for each extra hit scored, so you could\nexchange 2 S4 hits for a single S5 hit, or 3 S4 hits for\na single S6 hit, and so on.\n','Muscle'),(23,'Hurl Opponent. ','If you win a round of combat, instead of hitting your\nopponent you can throw him D6\" in the direction of\nyour choice. The thrown model takes a single hit\nequal to half the distance rolled. If it hits a solid object\n(such as a wall) before it reaches the full distance\nthrown it will stop there. If it hits another model, then\nboth models take a hit equal to half the distance\nrolled. Note that the best way to use this skill is to\nthrow opposing models off tall buildings!\n','Muscle'),(24,'Iron Jaw. ','If a model with this skill is hit in hand-to-hand combat\nreduce the strength of each hit suffered by 1 point.\n','Muscle'),(25,'Berserk Charge. ','A model with this skill rolls double the number of Attack \ndice on its profile in the turn when it charges. However, \na model making a berserk charge may not parry that turn. \n','Ferocity'),(26,'Impetuous. ','If a model has this skill he may increase the range of his\nfollow-up move in hand-to-hand combat from\n2\" to 4\" ','Ferocity'),(27,'Iron Will. ','Only the gang leader may have this skill. It allows you to\nre-roll a failed Bottle roll as long as the leader is not down\nor out of action.\n','Ferocity'),(28,'Killer Reputation. ','A model with this skill has such a reputation as a vicious\nand depraved killer that his foes quail when he charges\nthem. This causes fear and the enemy must take a\npsychology test for fear as appropriate.\n','Ferocity'),(29,'Nerves of Steel. ','If the model fails a dice roll to avoid being pinned it may\nmake the roll again.\n','Ferocity'),(30,'True Grit. ','Treat a roll of 1 or 2 as a flesh wound when rolling for the\nextent of injuries. A roll of 3-5 indicates the model has\ngone down, and a roll of 6 means it is out of action, as\nnormal. When using special injury charts (needle guns,\nfor example) add 1 to the lowest result band in the same\nway as above.\n','Ferocity'),(31,'Crack Shot. ','A model with this skill can re-roll the Injury dice when\nrolling injuries he has inflicted by shooting. You must\naccept the result of the second roll regardless of the\nresult.\n','Shooting'),(32,'Fast Shot. ','If a model has Fast Shot skill he may shoot several\ntimes in the shooting phase and not just once as\nnormal. The model can shoot as many times as his\nAttacks characteristic. He can shoot at the same target\nor at separate targets as you wish.\nThis skill may only be used with pistols and basic\nweapons. It may not be used with special or heavy\nweapons as they are far too cumbersome.\n','Shooting'),(33,'Gunfighter. ','The model can aim and fire a pistol from each hand.\nThis enables him to take two shots in the shooting\nphase if he carries two pistols. If he carries a basic,\nspecial or heavy weapon he always requires one hand\nto hold this and so cannot use two pistols at once.\n','Shooting'),(34,'Hip Shooting. ','The model is allowed to shoot even if it ran in the\nsame turn. However, if it does so it suffers a -1 to hit\nmodifier and cannot count any bonuses from sights.\nNote that it is impossible to run and shoot with a\nheavy weapon, even with the Hip Shooting skill.\n','Shooting'),(35,'Marksman. ','A model with the Marksman skill may ignore the\nnormal restriction which obliges fighters to shoot at\nthe nearest target. Instead, he can shoot at any target\nhe can see.\nIn addition, a model with the Marksman skill may\nshoot at targets at extreme range – this is between\nnormal maximum range and half as far again. For\nexample, a lasgun has a normal maximum range of\n24\n\" and an extreme range of 24-36\". Shots at extreme\nrange suffer the same ‘to hit’ penalty as long range.\nThis skill may only be used with basic weapons. It may\nnot be used with pistols, special or heavy weapons.\n','Shooting'),(36,'Rapid Fire. ','If the model does not move in its movement phase it\ncan shoot twice in the shooting phase. This skill only\nworks with one specified kind of pistol or basic\nweapon which you must choose when the skill is\nearned. Note this down on the gang roster – eg, Rapid\nFire/Bolt pistol.\n','Shooting'),(37,'Armourer. ','The armourer checks all the weapons being used by the\ngang before the battle starts. Any model in the gang may\nadd +1 to any and all Ammo rolls (including a roll to\ndetermine if a weapon explodes). A roll of 1 is always a\nfailure regardless.\n','Techno'),(38,'Fixer. ','Gangers only. If the model is used to work a piece of\nterritory with a randomly generated income, you may re-\nroll the dice if you do not like the first result. You must\naccept the result of the second roll.\n','Techno'),(39,'Inventor. ','Roll a D6 after each battle. On a roll of 6 the model has\ninvented something! Randomly select an item from the\nrare Trade Chart in the Trading Post section (page 97).\nWhatever is selected is the item that has been invented.\n','Techno'),(40,'Medic. ','The model has some experience of patching up his fellow\nfighters. If your gang includes a fighter with this skill you\ncan re-roll a result on the Serious Injury table for one\nmodel after a battle.\n','Techno'),(41,'Specialist. ','This skill may only be taken by juves or gangers. It allows\nthe model to be armed with a special weapon.\n','Techno'),(42,'Weaponsmith. ','A model with this skill may ignore failed Ammo rolls and\nweapon explosions on a D6 roll of 4+.\n','Techno');
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stash`
--

DROP TABLE IF EXISTS `stash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stash` (
  `stashId` int(11) NOT NULL AUTO_INCREMENT,
  `weaponId` int(11) NOT NULL,
  `gangId` int(11) NOT NULL,
  `cost` int(11) DEFAULT NULL,
  PRIMARY KEY (`stashId`),
  KEY `weaponId` (`weaponId`),
  KEY `gangId` (`gangId`),
  CONSTRAINT `stash_ibfk_1` FOREIGN KEY (`weaponId`) REFERENCES `weapons` (`weaponId`),
  CONSTRAINT `stash_ibfk_2` FOREIGN KEY (`gangId`) REFERENCES `gangs` (`gangId`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stash`
--

LOCK TABLES `stash` WRITE;
/*!40000 ALTER TABLE `stash` DISABLE KEYS */;
INSERT INTO `stash` VALUES (30,68,2,63),(32,12,3,20),(37,28,1,40);
/*!40000 ALTER TABLE `stash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `territories`
--

DROP TABLE IF EXISTS `territories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `territories` (
  `territoryId` int(11) NOT NULL AUTO_INCREMENT,
  `territoryName` varchar(50) DEFAULT NULL,
  `income` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`territoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `territories`
--

LOCK TABLES `territories` WRITE;
/*!40000 ALTER TABLE `territories` DISABLE KEYS */;
INSERT INTO `territories` VALUES (1,'Chem Pit','2D6'),(2,'Old Ruins','10'),(3,'Slag','15'),(4,'Mineral Outcrop','D6*10'),(5,'Settlement','30'),(6,'Mine Workings','D6*10'),(7,'Tunnels','10'),(8,'Vents','10'),(9,'Holestead','D6*10'),(10,'Water Still','D6*10'),(11,'Drinking Hole','D6*10'),(12,'Guilder Contact','D6*10'),(13,'Friendly Doc','D6*10'),(14,'Workshop','D6*10'),(15,'Gambling Den','2D6*10'),(16,'Spore Cave','2D6*10'),(17,'Archeotech','2D6*10');
/*!40000 ALTER TABLE `territories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weapons`
--

DROP TABLE IF EXISTS `weapons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weapons` (
  `weaponId` int(11) NOT NULL AUTO_INCREMENT,
  `weaponName` varchar(50) DEFAULT NULL,
  `shortRange` varchar(20) DEFAULT '',
  `longRange` varchar(20) DEFAULT '',
  `hitShort` varchar(20) DEFAULT '',
  `hitLong` varchar(20) DEFAULT '',
  `strength` varchar(20) DEFAULT '',
  `damage` varchar(20) DEFAULT '',
  `saveMod` varchar(20) DEFAULT '',
  `ammoRoll` varchar(20) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `cost` varchar(20) DEFAULT '',
  `availability` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`weaponId`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weapons`
--

LOCK TABLES `weapons` WRITE;
/*!40000 ALTER TABLE `weapons` DISABLE KEYS */;
INSERT INTO `weapons` VALUES (1,'Massive Axe','CC','CC','-','-','user +2','1','-','-','Hand to Hand','15','common'),(2,'Club','CC','CC','-','-','user +1','1','-','-','Hand to Hand','10','common'),(3,'Chains','CC','CC','-','-','user +1','1','-','-','Hand to Hand','10','common'),(4,'Chainsword','CC','CC','-','-','4','1','-1','-','Hand to Hand','25','common'),(5,'Sword','CC','CC','-','-','User','1','-','-','Hand to Hand','10','common'),(6,'Power Sword','CC','CC','-','-','5','1','-3','-','Hand to Hand','40+3D6','rare'),(7,'Power Axe (2-hand)','CC','CC','-','-','6','1','-3','-','Hand to Hand','35+3D6','rare'),(8,'Power Axe (1-hand)','CC','CC','-','-','5','1','-2','-','Hand to Hand','35+3D6','rare'),(9,'Power Fist','CC','CC','-','-','8','1','-5','-','Hand to Hand','85+3D6','rare'),(10,'Power Maul','CC','CC','-','-','5','1','-3','-','Hand to Hand','35+3D6','rare'),(11,'Autopistol','0-8','8-16','+2','-','3','1','-','4+','Pistols','15','common'),(12,'Boltpistol','0-8','8-16','+2','-','4','1','-1','6+','Pistols','20','common'),(13,'Laspistol','0-8','8-16','+2','-1','3','1','-','2+','Pistols','15','common'),(14,'Needle Pistol','0-8','8-16','+2','-','3','1','-1','6+','Pistols','100+4D6','rare'),(15,'Plasma Pistol(Max power)','0-6','6-18','+2','-1','6','1','-3','4+','Pistols','25','common'),(16,'Plasma Pistol(Low power)','0-6','6-18','+2','-1','4','1','-1','4+','Pistols','25','common'),(17,'Stub Gun','0-8','8-16','-','-1','3','1','-','4+','Pistols','10','common'),(18,'Web pistol','0-4','4-8','-','-1','-','special','-','6+','Pistols','120+4D6','rare'),(19,'Hand Flamer','Template','Template','-','-','4','1','-2','Special','Pistols','20','common'),(20,'Autogun','0-12','12-24','+1','-','3','1','-','4+','Basic','20','common'),(21,'Boltgun','0-12','12-24','+1','-','4','1','-1','6+','Basic','35','common'),(22,'Lasgun','0-12','12-24','+1','-','3','1','-1','2+','Basic','25','common'),(23,'Shotgun (solid slug)','0-4','4-18','-','-1','4','1','-','4+','Basic','20','common'),(24,'Shotgun (scatter)','0-4','4-18','+1','-1','3','1','-','4+','Basic','20','common'),(25,'Man stopper shell','0-4','4-18','-','-','4','1','-2','4+','Basic','5','common'),(26,'Hot shot shell','0-4','4-18','-','-1','4','1','-','6+','Basic','5','common'),(27,'Bolt shell','0-4','4-18','+1','-','4','1','-1','6+','Basic','15','common'),(28,'Flamer','Template','Template','-','-','4','1','-2','4+','Basic','40','common'),(29,'Grenade Launcher','0-20','20-60','-','-1','-','-','-','auto','Basic','130','common'),(30,'Melta-gun','0-6','6-12','+1','-','8','D6','-4','4+','Basic','95','common'),(31,'Needle Rifle','0-16','16-32','+1','-','3','1','-1','6+','Basic','230+4D6','rare'),(32,'Plasma Gun (low energy)','0-6','6-16','+1','-','5','1','-1','4+','Basic','70','common'),(33,'Plasma Gun (max energy)','0-6','6-24','+1','-','7','1','-2','4+','Basic','70','common'),(34,'Autocannon','0-20','20-72','-','-','8','D6','-3','4+','Heavy','300','common'),(35,'Heavy Bolter','0-20','20-40','-','-','5','D3','-2','6+','Heavy','180','common'),(36,'Heavy Plasma Gun (low)','0-20','20-40','-','-','7','D3','-2','4+','Heavy','285','common'),(37,'Heavy Plasma Gun (max)','0-20','20-72','-','-','10','D6','-6','4+','Heavy','285','common'),(38,'Heavy Stubber','0-20','20-40','-','-','4','1','-1','4+','Heavy','120','common'),(39,'Lascannon','0-20','20-60','-','-','9','2D6','-6','4+','Heavy','400','common'),(40,'Missile Launcher (Krak)','0-20','20-72','-','-','8','D6','-6','auto','Heavy','220','common'),(41,'Missile Launcher (Frag)','0-20','20-72','-','-','4','1','-1','auto','Heavy','300','common'),(42,'Krak Grenade','-','-','-','-','6','D6','-3','-','Grenade','50','common'),(43,'Frag Grenade','-','-','-','-','3','1','-1','-','Grenade','30','common'),(44,'Melta Bomb','-','-','-','-','8','D6','-4','-','Grenade','40+3D6','rare'),(45,'Plasma Grenade','-','-','-','-','5','1','-2','-','Grenade','30+3D6','rare'),(46,'Photon Flash Flare','-','-','-','-','0','0','0','-','Grenade','20+2D6','rare'),(47,'Carapace','','','','','','','4+','','Armour','70+3D6','rare'),(48,'Flak','','','','','','','6+/5+','','Armour','10+2D6','rare'),(49,'Mesh','','','','','','','5+','','Armour','25+3D6','rare'),(50,'Hotshot Power Pack','','','','','','','','','Ammo/Sights','15','rare'),(51,'Infra-red sight','','','','','','','','','Ammo/Sights','30+3D6','rare'),(52,'Mono-sight','','','','','','','','','Ammo/Sights','40+3D6','rare'),(53,'Red-dot sight','','','+1','+1','','','','','Ammo/Sights','40+3D6','rare'),(54,'Telescopic Sight','','','','','','','','','Ammo/Sights','40+3D6','rare'),(55,'Bionic Eye','','','','','','','','','Bionics','50+3D6','rare'),(56,'Bionic Arm','','','','','','','','','Bionics','80+3D6','rare'),(57,'Bionic Leg','','','','','','','','','Bionics','80+3D6','rare'),(58,'Auto-Repairer','','','','','','','','','Misc','80+4D6','rare'),(59,'Bio-Booster','','','','','','','','','Misc','50+4D6','rare'),(60,'Bio-Scanner','','','','','','','','','Misc','50+3D6','rare'),(61,'Blindsnake Pouch','','','','','','','','','Misc','30+2D6','rare'),(62,'Clip Harness','','','','','','','','','Misc','10','common'),(63,'Concealed Blade','','','','','','','','','Misc','10+D6','rare'),(64,'Filter Plugs','','','','','','','','','Misc','10','common'),(65,'Grav Chute','','','','','','','','','Misc','40+4D6','rare'),(66,'Grapnel','','','','','','','','','Misc','30+4D6','rare'),(67,'Infra-Goggles','','','','','','','','','Misc','30+3D6','rare'),(68,'Isotropic Fuel Rod','','','','','','','','','Misc','50+4D6','rare'),(69,'Lobo-chip','','','','','','','','','Misc','20','common'),(70,'Medi-pack','','','','','','','','','Misc','80+4D6','rare'),(71,'Mung Vase','','','','','','','','','Misc','10*D6','rare'),(72,'Photo-contacts','','','','','','','','','Misc','15','common'),(73,'Ratskin Map','','','','','','','','','Misc','10*D6','rare'),(74,'Respirator','','','','','','','','','Misc','10','common'),(75,'Screamers (x1)','','','','','','','','','Misc','10+3D6','rare'),(76,'Silencer','','','','','','','','','Misc','10+2D6','rare'),(77,'Skull Chip','','','','','','','','','Misc','30+3D6','rare'),(78,'Stummers (x1)','','','','','','','','','Misc','10+3D6','rare');
/*!40000 ALTER TABLE `weapons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-18 14:23:13
