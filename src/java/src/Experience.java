package src;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class handles all of the post game actions: injuries, experience,
 * income.
 *
 * @author weirdvector
 */
public class Experience {

    private Random r;
    private Skills skills;
    private String report = "";

    public Experience() {

    }

    public String getReport() {
        return report;
    }

    /**
     * Calculate experience for a Ganger, and do appropriate advances. This is
     * the PostGame class's only public method, and the so is the only method
     * that will be called when pushing the Post-Game button.
     *
     * @param ganger The ganger who is being advance.
     * @param gameType The game's scenario, determining the bonuses, objectives,
     * etc.
     * @param hasWon True if the player won.
     * @param obj1 Representing an achieved objective.
     * @param obj2 Representing an achieved objective.
     * @param obj3 Representing an achieved objective.
     * @param woundingHit1 Representing the Ganger having downed an opponent
     * model.
     * @param woundingHit2 Representing the Ganger having downed an opponent
     * model.
     * @param woundingHit3 Representing the Ganger having downed an opponent
     * model.
     * @param isAttacker Whether or not the player was attacking in the
     * scenario.
     * @param opponentGangRating The opponent gang's gang rating.
     * @param gangRating The player gang's gang rating.
     * @param gangHouse The house of the gang, relevant for advance rolls.
     * @return The levelled up Ganger.
     */
    public Ganger calcExperience(Ganger ganger, String gameType, boolean hasWon,
            boolean obj1, boolean obj2, boolean obj3, boolean woundingHit1,
            boolean woundingHit2, boolean woundingHit3, boolean isAttacker,
            int opponentGangRating, int gangRating, String gangHouse) {
        report = "";
        r = new Random();
        int exp = 0;
        int currentExp = ganger.getExp();
        switch (gameType.toLowerCase()) {
            case "gangfight":
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (ganger.getGangerType() == 'L' && hasWon) {
                    exp += 10;
                }
                exp += (r.nextInt(6) + 1);
                break;
            case "scavengers":
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (obj1) {
                    exp += 1;
                }
                if (obj2) {
                    exp += 1;
                }
                if (obj3) {
                    exp += 1;
                }
                if (ganger.getGangerType() == 'L' && hasWon) {
                    exp += 10;
                }
                exp += (r.nextInt(6) + 1);
                break;
            case "hit and run":
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (isAttacker && hasWon) {
                    exp += 10;
                }
                exp += (r.nextInt(6) + 1);
                break;
            case "ambush":
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (ganger.getGangerType() == 'L' && hasWon) {
                    exp += 10;
                }
                exp += (r.nextInt(6) + 1);
                break;
            case "the raid":
                if (!isAttacker && hasWon && ganger.getGangerType() == 'L') {
                    exp += 10;
                }
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (obj1) {
                    exp += 5;
                }
                if (obj2) {
                    exp += 5;
                }
                if (obj3) {
                    exp += 5;
                }
                exp += (r.nextInt(6) + 1);
                break;
            case "rescue":
                if (!isAttacker && hasWon && ganger.getGangerType() == 'L') {
                    exp += 10;
                }
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (obj1) {
                    exp += 5;
                }
                if (obj2) {
                    exp += 5;
                }
                if (obj3) {
                    exp += 5;
                }
                exp += (r.nextInt(6) + 1);
                break;
            case "shootout":
                if (woundingHit1) {
                    exp += 5;
                }
                if (woundingHit2) {
                    exp += 5;
                }
                if (woundingHit3) {
                    exp += 5;
                }
                if (hasWon) {
                    exp += 5;
                }
                exp += (r.nextInt(6) + 1);
                break;
        }

        //check for levelup here!
        int oppExp = opponentExpBonus(opponentGangRating, gangRating, hasWon);
        ganger = levelUp(ganger, currentExp, exp + oppExp, gangHouse);
        ganger = rankUp(ganger);
        return ganger;
    }

    /**
     * Determine the bonus for winning/losing against a more highly-rated gang.
     *
     * @param opponentGangRating The opponent gang's gang rating
     * @param gangRating The player's gang rating
     * @param hasWon Whether or not the player won the game (true on win)
     * @return An int representing the bonus achieved.
     */
    private int opponentExpBonus(int opponentGangRating, int gangRating, boolean hasWon) {
        int diff = Math.abs(opponentGangRating - gangRating);
        int exp = 0;
        if (diff <= 49) {
            if (hasWon) {
                exp += 1;
            } else {
                exp += 0;
            }
        } else if (diff >= 50 && diff <= 99) {
            if (hasWon) {
                exp += 2;
            } else {
                exp += 1;
            }
        } else if (diff >= 100 && diff <= 149) {
            if (hasWon) {
                exp += 3;
            } else {
                exp += 2;
            }
        } else if (diff >= 150 && diff <= 199) {
            if (hasWon) {
                exp += 4;
            } else {
                exp += 3;
            }
        } else if (diff >= 200 && diff <= 249) {
            if (hasWon) {
                exp += 5;
            } else {
                exp += 4;
            }
        } else if (diff >= 250 && diff <= 499) {
            if (hasWon) {
                exp += 6;
            } else {
                exp += 5;
            }
        } else if (diff >= 500 && diff <= 749) {
            if (hasWon) {
                exp += 7;
            } else {
                exp += 6;
            }
        } else if (diff >= 750 && diff <= 999) {
            if (hasWon) {
                exp += 8;
            } else {
                exp += 7;
            }
        } else if (diff >= 1000 && diff <= 1499) {
            if (hasWon) {
                exp += 9;
            } else {
                exp += 8;
            }
        } else if (diff >= 1500) {
            if (hasWon) {
                exp += 10;
            } else {
                exp += 9;
            }
        }
        return exp;
    }

    /**
     * Determine the number of advance rolls, then call that method an
     * appropriate number of times.
     *
     * @param currentExp The ganger's experience before levelling up.
     * @param exp The experience achieved in the previous game
     * @return The levelled up Ganger.
     */
    public Ganger levelUp(Ganger ganger, int currentExp, int exp, String gangHouse) {
        int levelUps = 0;
        int limit = 0;
        //experience advance chart here
        while (exp > 0) {
            if (currentExp <= 5) {
                limit = 6;
            } else if (currentExp > 5 && currentExp <= 10) {
                limit = 11;
            } else if (currentExp > 10 && currentExp <= 15) {
                limit = 16;
            } else if (currentExp > 15 && currentExp <= 20) {
                limit = 21;
            } else if (currentExp > 20 && currentExp <= 30) {
                limit = 31;
            } else if (currentExp > 30 && currentExp <= 40) {
                limit = 41;
            } else if (currentExp > 40 && currentExp <= 50) {
                limit = 51;
            } else if (currentExp > 50 && currentExp <= 60) {
                limit = 61;
            } else if (currentExp > 60 && currentExp <= 80) {
                limit = 81;
            } else if (currentExp > 80 && currentExp <= 100) {
                limit = 101;
            } else if (currentExp > 100 && currentExp <= 120) {
                limit = 121;
            } else if (currentExp > 120 && currentExp <= 140) {
                limit = 141;
            } else if (currentExp > 140 && currentExp <= 160) {
                limit = 161;
            } else if (currentExp > 160 && currentExp <= 180) {
                limit = 181;
            } else if (currentExp > 180 && currentExp <= 200) {
                limit = 201;
            } else if (currentExp > 200 && currentExp <= 240) {
                limit = 241;
            } else if (currentExp > 240 && currentExp <= 280) {
                limit = 281;
            } else if (currentExp > 280 && currentExp <= 320) {
                limit = 321;
            } else if (currentExp > 320 && currentExp <= 360) {
                limit = 361;
            } else if (currentExp > 360 && currentExp <= 400) {
                limit = 401;
            } else {
                //You'll never get a ganger this high level, so it's set ridiculously high.
                limit = Integer.MAX_VALUE;
            }

            //calculate the number of advance rolls
            if ((currentExp + exp) > limit) {
                exp = currentExp + exp - limit;
                currentExp = limit;
                levelUps++;
            } else {
                currentExp += exp;
                exp = 0;
            }
        }

        for (int i = 0; i < levelUps; i++) {
            report = "<br><strong>" + ganger.getName() + "</strong> has levelled up.<br>";
            ganger = advanceRolls(ganger, gangHouse);
        }
        ganger.setExp(currentExp);
        return ganger;
    }

    private Ganger advanceRolls(Ganger ganger, String gangHouse) {
        r = new Random();
        skills = new Skills();
        ArrayList<String> options = new ArrayList(0);
        //advance rolls are determined by adding 2D6
        int roll = (r.nextInt(6) + 1) + (r.nextInt(6) + 1);
        switch (roll) {
            case 2:
            case 12:
                //any skill;
                report += "<select name='pickAnySkill'" + ganger.getGangerId() + ">" +
                        "<option>Select Any Skill</option>" +
                        "<option value='agility'>Agility</option>" +
                        "<option value='combat'>Combat</option>" +
                        "<option value='stealth'>Stealth</option>" +
                        "<option value='muscle'>Muscle</option>" +
                        "<option value='ferocity'>Ferocity</option>" +
                        "<option value='shooting'>Shooting</option>" +
                        "<option value='techno'>Techno</option>" +
                        "</select><br>";
                break;
            case 3:
            case 4:
            case 10:
            case 11:
                //gang skill
                                
                //set up the select tag and options according to gang house
                //skills list, and ganger type
                report += "<select name='pickGangSkill" + ganger.getGangerId() + "'>";
                if (gangHouse.toLowerCase().contains("cawdor")) {
                    switch (ganger.getGangerType()) {
                        case 'J':
                            report += "<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            break;
                        case 'G':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            break;
                        case 'H':
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'L':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                    }
                } else if (gangHouse.toLowerCase().contains("escher")) {
                    switch (ganger.getGangerType()) {
                        case 'J':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            break;
                        case 'G':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            break;
                        case 'H':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'L':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                    }
                } else if (gangHouse.toLowerCase().contains("delaque")) {
                    switch (ganger.getGangerType()) {
                        case 'J':
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            break;
                        case 'G':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            break;
                        case 'H':
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'L':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                    }
                } else if (gangHouse.toLowerCase().contains("goliath")) {
                    switch (ganger.getGangerType()) {
                        case 'J':
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            break;
                        case 'G':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            break;
                        case 'H':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'L':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                    }
                } else if (gangHouse.toLowerCase().contains("orlock")) {
                    switch (ganger.getGangerType()) {
                        case 'J':
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            break;
                        case 'G':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            break;
                        case 'H':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'L':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                    }
                } else if (gangHouse.toLowerCase().contains("van saar")) {
                    switch (ganger.getGangerType()) {
                        case 'J':
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'G':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'H':
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='muscle'>Muscle</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                        case 'L':
                            report +="<option value='agility'>Agility</option>";
                            report +="<option value='combat'>Combat</option>";
                            report +="<option value='ferocity'>Ferocity</option>";
                            report +="<option value='shooting'>Shooting</option>";
                            report +="<option value='stealth'>Stealth</option>";
                            report +="<option value='techno'>Techno</option>";
                            break;
                    }
                }
                report +="</select><br>";
                break;
            case 5:
                roll = (r.nextInt(2));
                if (roll == 0) {
                    //+1 strength
                    ganger.setStrength(ganger.getStrength() + 1);
                    report += "Strength increased: +1<br>";
                } else if (roll == 1) {
                    //+1 attack
                    ganger.setAttack(ganger.getAttack() + 1);
                    report += "Attack increased: +1<br>";
                }
                break;
            case 6:
            case 8:
                roll = (r.nextInt(2));
                if (roll == 0) {
                    //+1 WS
                    ganger.setWs(ganger.getWs() + 1);
                    report += "WS increased: +1<br>";
                } else if (roll == 1) {
                    //+1 BS
                    ganger.setBs(ganger.getBs() + 1);
                    report += "BS increased: +1<br>";
                }
                break;
            case 7:
                roll = (r.nextInt(2));
                if (roll == 0) {
                    //+1 Initiative
                    ganger.setInitiative(ganger.getInitiative() + 1);
                    report += "Initiative increased: +1<br>";
                } else if (roll == 1) {
                    //+1 leadership
                    ganger.setLeadership(ganger.getLeadership() + 1);
                    report += "Leadership increased: +1<br>";
                }
                break;
            case 9:
                roll = (r.nextInt(2));
                if (roll == 0) {
                    //+1 wounds
                    ganger.setWounds(ganger.getWounds() + 1);
                    report += "Wounds increased: +1<br>";
                } else if (roll == 1) {
                    //+1 attacks
                    ganger.setAttack(ganger.getAttack() + 1);
                    report += "Attacks increased: +1<br>";
                }
                break;
        }
        return ganger;
    }

    /**
     * Check to see if a Juve has enough experience to become a Ganger.
     *
     * @param ganger The ganger being checked.
     * @return A ganger upgraded to Ganger status, if previously a Juve.
     */
    private Ganger rankUp(Ganger ganger) {
        if (ganger.getExp() >= 21 && ganger.getGangerType() == 'J') {
            ganger.setGangerType('G');
            report += "Rank increased to Ganger.<br>";
        }
        return ganger;
    }

}
