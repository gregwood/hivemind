package src;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author weirdvector
 */

public class Gang {
    
    private String gangName;
    private ArrayList<Ganger> gang;
    private String gangHouse;
    private int credits;
    private Iterator gangIterator;
    private int gangId;

    public int getGangId() {
        return gangId;
    }

    public void setGangId(int gangId) {
        this.gangId = gangId;
    }
    
    public String getGangName() {
        return gangName;
    }
    
    public void setGangName(String gangName) {
        this.gangName = gangName;
    }
    
    public String getGangHouse() {
        return gangHouse;
    }

    public void setGangHouse(String gangHouse) {
        this.gangHouse = gangHouse;
    }
    
    public Gang() {
        gang = new ArrayList(0);
    }
    
    public void addGanger(Ganger ganger) {
        gang.add(ganger);
    }
    
    public Ganger nextGanger() {
        if (gangIterator == null) {
            makeIterator();
        }
        
        return (Ganger)gangIterator.next();
    }
    
    public void makeIterator() {
        gangIterator = gang.iterator();
    }
    
    public boolean hasMoreGangers() {
        if (gangIterator == null) {
            makeIterator();
        }
        return gangIterator.hasNext();
    }
    
    public int getGangRating() {
        int rating = 0;
        for (Ganger g : gang) {
            rating += g.getCost() + g.getExp();
        }
        return rating;
    }
    
    public ArrayList<Ganger> getGangArrayList() {
        return gang;
    }
    
    public void setGangArrayList(ArrayList<Ganger> gangList) {
        gang = gangList;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
    
}
