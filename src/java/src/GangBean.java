package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * This class is responsible for reading from and writing from the database.
 *
 * @author weirdvector
 */
public class GangBean {

    private Connection con;
    private Statement st;
    private PreparedStatement ps;
    private ResultSet rs;

    /**
     * Constructor for the Bean class used to connect to the database.
     */
    public GangBean() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        String url = "jdbc:mysql://localhost/hivemind";
        con = DriverManager.getConnection(url, "hivemind", "necromunda"); //url, user, pass
    }

    /**
     * Reads the database and creates a Gang object populated with Gangers.
     * @param gangId The database ID of the gang to created.
     * @return The created Gang object.
     */
    public Gang getGang(int gangId) throws SQLException {
        //Create the Gang
        Gang gang = new Gang();
        
        //set gang information: name and house.
        
        String query = "SELECT * FROM gangs WHERE gangId = " + gangId + ";";
        st = con.createStatement();
        rs = st.executeQuery(query);

        while (rs.next()) {
            gang.setGangName(rs.getString("gangName"));
            gang.setGangHouse(rs.getString("house"));
            gang.setCredits(rs.getInt("credits"));
            gang.setGangId(rs.getInt("gangId"));
        }
        
        //add all of the living gangers to the gang
        query = "SELECT * FROM gangers WHERE gangId = " + gangId + " AND dead = 0;";
        st = con.createStatement();
        rs = st.executeQuery(query);

        while (rs.next()) {
            Ganger g = new Ganger();
            g.setGangerId(rs.getInt("gangerId"));
            g.setName(rs.getString("name"));
            g.setMove(rs.getInt("move"));
            g.setWs(rs.getInt("ws"));
            g.setBs(rs.getInt("bs"));
            g.setStrength(rs.getInt("s"));
            g.setToughness(rs.getInt("t"));
            g.setWounds(rs.getInt("w"));
            g.setInitiative(rs.getInt("i"));
            g.setAttack(rs.getInt("a"));
            g.setLeadership(rs.getInt("ld"));
            g.setCost(rs.getInt("cost"));
            g.setExp(rs.getInt("experience"));
            g.setInjuries(rs.getString("injuries"));
            g.setSkills(rs.getString("skills"));
            g.setGangerType(rs.getString("type").charAt(0));
            g.setGangId(gangId);
            gang.addGanger(g);
        }

        rs.close();
        st.close();
            
        return gang;
    }

    /**
     * Given a Gang object, this updates the database.
     * @param gang The Gang to update.
     */
    public void writeSheet(Gang gang) throws SQLException {
        //add the Ganger's info to the database
        while (gang.hasMoreGangers()) {
            Ganger g = gang.nextGanger();

            updateGanger(g);

        }
    }
    
    /**
     * Updates the stats of a particular Ganger in the database.
     * @param g The Ganger to update.
     * @throws SQLException
     */
    public void updateGanger(Ganger g) throws SQLException {
        st = con.createStatement();
        String statement = "UPDATE gangers SET name = '" + g.getName() + "', "
                + "gangId = " + g.getGangId() + ", move = " + g.getMove() + ", "
                + "ws = " + g.getWs() + ", bs = " + g.getBs()
                + ", s = " + g.getStrength() + ", t = " + g.getToughness() + ", "
                + " w = " + g.getWounds() + ", i = " + g.getInitiative()
                + ", a = " + g.getAttack() + ", ld = " + g.getLeadership() 
                + ", skills = '" + g.getSkills() + "', injuries = '" + g.getInjuries()
                + "', cost = " + g.getCost() + ", experience = " + g.getExp()
                + ", type = '" + g.getGangerType() + "', dead = " + g.isDead()
                + " WHERE gangerId = " + g.getGangerId() + ";";
        st.executeUpdate(statement);
        st.close();
    }
    
    public int addGang(Gang gang) throws SQLException {
        st = con.createStatement();
        String statement = "INSERT INTO gangs(gangName, house, credits) VALUES "
                + "('" + gang.getGangName() + "', '" + gang.getGangHouse() 
                + "', " + gang.getCredits() + ");";
        st.executeUpdate(statement);
        st.close();
        
        st = con.createStatement();
        statement = "SELECT * FROM gangs WHERE gangName = '" + gang.getGangName() + "';";
        rs = st.executeQuery(statement);
        int gangId = 0;
        
        while (rs.next()) {
            gangId = rs.getInt("gangId");
        }
        return gangId;
    }
    
    /**
     * Writes new Ganger to the database.
     * @param g The Ganger to create. 
     * @throws SQLException
     */
    public void addGanger(Ganger g) throws SQLException {
        st = con.createStatement();
        String statement = "INSERT INTO gangers(gangId, name, move, ws, bs, s, t, w, i, a, ld, "
                + "skills, injuries, cost, experience, type, dead) VALUES (" + g.getGangId() 
                + ", '" + g.getName() + "', " + g.getMove() + ", " + g.getWs() + ", " + g.getBs()
                + ", " + g.getStrength() + ", " + g.getToughness() + ", " + g.getWounds()
                + ", " + g.getInitiative() + ", " + g.getAttack() + ", " + g.getLeadership()
                + ", '" + g.getSkills() + "', '" + g.getInjuries() + "', " + g.getCost() + ", "
                + g.getExp() + ", '" + g.getGangerType() + "', " + g.isDead() + ");";
        st.executeUpdate(statement);
        st.close();
        
        //charge for the ganger.
        st = con.createStatement();
        statement = "UPDATE gangs SET credits = (SELECT credits - " + g.getCost() + ") "
                + "WHERE gangId = " + g.getGangId() + ";";
        st.executeUpdate(statement);
        st.close();
    }
    
    /**
     * Reads from the gangers table in the database and creates a Ganger object.
     * @param id The gangerId of the Ganger.
     * @return The requested Ganger.
     */
    public Ganger getGanger(String id) throws SQLException {
        Ganger g = new Ganger();
        String statement = "SELECT * FROM gangers WHERE `gangerId` = " + id + ";";
        st = con.createStatement();
        rs = st.executeQuery(statement);

        while (rs.next()) {
            g.setGangerId(rs.getInt("gangerId"));
            g.setName(rs.getString("name"));
            g.setMove(rs.getInt("move"));
            g.setWs(rs.getInt("ws"));
            g.setBs(rs.getInt("bs"));
            g.setStrength(rs.getInt("s"));
            g.setToughness(rs.getInt("t"));
            g.setWounds(rs.getInt("w"));
            g.setInitiative(rs.getInt("i"));
            g.setAttack(rs.getInt("a"));
            g.setLeadership(rs.getInt("ld"));
            g.setCost(rs.getInt("cost"));
            g.setExp(rs.getInt("experience"));
            g.setInjuries(rs.getString("injuries"));
            g.setSkills(rs.getString("skills"));
            g.setGangId(rs.getInt("gangId"));
            g.setGangerType(rs.getString("type").charAt(0));
        }
        rs.close();
        return g;
    }
    
    /**
     * Returns information about all Gangs.
     * @return An Arraylist of all Gangs.
     * @throws SQLException 
     */
    public ArrayList<Gang> getGangs() throws SQLException {
        
        ArrayList<Gang> ganglist = new ArrayList();
        
        st = con.createStatement();
        String statement = "SELECT * FROM gangs";
        rs = st.executeQuery(statement);
        
        while (rs.next()) {
            Gang gang = new Gang();
            gang.setGangName(rs.getString("gangName"));
            gang.setGangId(rs.getInt("gangId"));
            gang.setCredits(rs.getInt("credits"));
            gang.setGangHouse(rs.getString("house"));
            
            ganglist.add(gang);
        }
        
        rs.close();
        st.close();
        
        ganglist.trimToSize();
        return ganglist;
    }
    
    /**
     * Returns a list of territories owned by Gang.
     * @param gangId The gang whose territory we want.
     * @return An array list of string arrays, in the format [territoryName][id]
     * @throws SQLException
     */
    public ArrayList<Territory> getTerritories(int gangId) throws SQLException {
        ArrayList<Territory> territories = new ArrayList();
        
        st = con.createStatement();
        String statement = "SELECT t.territoryName, t.income FROM territories t "
                + "INNER JOIN income i ON t.territoryId = i.territoryId "
                + "WHERE i.gangId = " + gangId + ";";

        rs = st.executeQuery(statement);
        
        while (rs.next() ) {
            Territory t = new Territory();
            t.setTerritoryName(rs.getString("territoryName"));
            t.setIncome(rs.getString("income"));
            territories.add(t);
        }
        
        rs.close();
        st.close();
        
        territories.trimToSize();
        return territories;
    }
    
    /**
     * Get the list of all weapons from weapons table.
     * @return An array list of all weapons as string array, index then name.
     * @throws java.sql.SQLException
     */
    public ArrayList<Weapon> getAllWeapons() throws SQLException {
        ArrayList<Weapon> weapons = new ArrayList();
        
        st = con.createStatement();
        String statement = "SELECT weaponId, weaponName FROM weapons";

        rs = st.executeQuery(statement);
        
        while (rs.next() ) {
            Weapon w = new Weapon();
            w.setWeaponId(rs.getInt("weaponId"));
            w.setWeaponName(rs.getString("weaponName"));
            weapons.add(w);
        }
        
        rs.close();
        st.close();
        
        weapons.trimToSize();
        return weapons;
    }
    
    /**
     * This method returns all the information for the weapons that are
     * currently in the gang's stash.
     * @param gangId The gang whose stash we're looking at.
     * @return An array list of String arrays that contain all weapon information.
     * @throws SQLException 
     */
    public ArrayList<Weapon> getStash(int gangId) throws SQLException {
        ArrayList<Weapon> stash = new ArrayList();
        
        st = con.createStatement();
        String statement = "SELECT w.weaponId, w.weaponName, w.cost FROM weapons w "
                + "INNER JOIN stash s ON w.weaponId = s.weaponId "
                + "INNER JOIN gangs g ON g.gangId = s.gangId "
                + "WHERE g.gangId = " + gangId + ";";
        rs = st.executeQuery(statement);
        
        while (rs.next() ) {
            Weapon w = new Weapon();
            w.setWeaponId(rs.getInt("weaponId"));
            w.setWeaponName(rs.getString("weaponName"));
            w.setCost(rs.getString("cost"));
            stash.add(w);
        }
        
        rs.close();
        st.close();
        
        stash.trimToSize();
        return stash;
    }
    
    /**
     * This method returns an ArrayList of all equipment whose availabilty
     * is common, for use in the store.
     * @return An arraylist of String arrays, where each String is an attribute of weapons.
     * @throws SQLException 
     */
    public ArrayList<Weapon> getCommonEquipment() throws SQLException {
        ArrayList<Weapon> commonEquipment = new ArrayList();
        st = con.createStatement();
        String statement = "SELECT * FROM weapons WHERE availability = \"common\";";
        
        rs = st.executeQuery(statement);
        
        while (rs.next()) {
            Weapon w = new Weapon();
            w.setWeaponId(rs.getInt(("weaponId")));
            w.setWeaponName(rs.getString("weaponName"));
            w.setShortRange(rs.getString("shortRange"));
            w.setLongRange(rs.getString("longRange"));
            w.setHitShort(rs.getString("hitShort"));
            w.setHitLong(rs.getString("hitLong"));
            w.setStrength(rs.getString("strength"));
            w.setDamage(rs.getString("damage"));
            w.setSaveMod(rs.getString("saveMod"));
            w.setAmmoRoll(rs.getString("ammoRoll"));
            w.setType(rs.getString("type"));
            w.setCost(rs.getString("cost"));
            commonEquipment.add(w);
        }
        
        rs.close();
        st.close();
        
        commonEquipment.trimToSize();
        return commonEquipment;
    }
    
    public ArrayList<Weapon> getRareEquipment() throws SQLException {
        ArrayList<Weapon> commonEquipment = new ArrayList();
        st = con.createStatement();
        String statement = "SELECT * FROM weapons WHERE availability = \"rare\";";
        
        rs = st.executeQuery(statement);
        
        while (rs.next()) {
            Weapon w = new Weapon();
            w.setWeaponId(rs.getInt(("weaponId")));
            w.setWeaponName(rs.getString("weaponName"));
            w.setShortRange(rs.getString("shortRange"));
            w.setLongRange(rs.getString("longRange"));
            w.setHitShort(rs.getString("hitShort"));
            w.setHitLong(rs.getString("hitLong"));
            w.setStrength(rs.getString("strength"));
            w.setDamage(rs.getString("damage"));
            w.setSaveMod(rs.getString("saveMod"));
            w.setAmmoRoll(rs.getString("ammoRoll"));
            w.setType(rs.getString("type"));
            w.setCost(rs.getString("cost"));
            commonEquipment.add(w);
        }
        
        rs.close();
        st.close();
        
        commonEquipment.trimToSize();
        return commonEquipment;
    }
    
    public void addWeaponToStash(int gangId, int weaponId, int cost) throws SQLException {
        st = con.createStatement();
        String statement = "INSERT INTO stash(gangId, weaponId, cost) VALUES "
                + "(" + gangId + ", " + weaponId + ", " + cost + ");";
        st.executeUpdate(statement);
        
        st.close();
    }
    
    public void removeFromStash(int gangId, int weaponId) throws SQLException {
        st = con.createStatement();
        
        //delete only one of the matching values from the stash
        String statement = "DELETE s.* FROM stash s WHERE stashId IN "
                + "(SELECT stashId FROM (SELECT stashId FROM stash "
                + "WHERE weaponId = " + weaponId + " AND gangId = " + gangId + " LIMIT 1) x);"; 
        st.execute(statement);
        st.close();
    }
    
    public void removeGangerEquipment(int weaponId, int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "DELETE e.* FROM equipment e WHERE equipmentId IN "
                + "(SELECT equipmentId FROM (SELECT equipmentId FROM equipment "
                + "WHERE weaponId = " + weaponId + " AND gangerId = " + gangerId + " LIMIT 1) x);";
        st.executeUpdate(statement);
        st.close();
    }
    
    /**
     * Equip one weapon to a ganger (add it to the equipment table)
     * @param weaponId The weapon to add.
     * @param g The ganger who is getting the weapon
     * @param cost The cost of the weapon.
     * @throws SQLException 
     */
    public void setEquipment(int weaponId, Ganger g, int cost) throws SQLException {
        st = con.createStatement();
        String statement = "INSERT INTO equipment (weaponId, gangerId, cost) VALUES "
                + "(" + weaponId + ", " + g.getGangerId() + ", " + cost + ");";
        st.executeUpdate(statement);
        st.close();
    }
    
    /**
     * Gets an individual ganger's equipment, for display on index page.
     * @param gangerId The ganger whose equipment we are viewing/
     * @return A string of all the equipment names.
     * @throws SQLException 
     */
    public ArrayList<Weapon> getGangerEquipment(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT w.weaponName, w.weaponId, e.cost FROM weapons w INNER JOIN "
                + "equipment e ON w.weaponId = e.weaponId "
                + "WHERE e.gangerId = " + gangerId + ";";
        rs = st.executeQuery(statement);
        
        ArrayList<Weapon> equipment = new ArrayList();
        while (rs.next()) {
            Weapon w = new Weapon();
            w.setWeaponName(rs.getString("weaponName"));
            w.setWeaponId(Integer.parseInt(rs.getString("weaponId")));
            w.setCost(rs.getString("cost"));
            equipment.add(w);
        }
        rs.close();
        st.close();
        
        equipment.trimToSize();
        return equipment;
    }
    
    /**
     * Get the list of all the unique weapons equipped to a gang 
     * to display in the weapon reference on index.jsp
     * @param gangId The gang id as an integer.
     * @return An arraylist of string arrays containing the weapon information.
     * @throws SQLException 
     */
    public ArrayList<Weapon> getGangEquipment(int gangId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT DISTINCT w.weaponName, w.shortRange, w.longRange, w.hitShort, " 
            + "w.hitLong, w.strength, w.damage, w.saveMod, w.ammoRoll FROM weapons w " 
            + "INNER JOIN equipment e ON w.weaponId = e.weaponId " 
            + "INNER JOIN gangers g ON e.gangerId = g.gangerId " 
            + "WHERE g.gangId = " + gangId + " AND g.dead = 0;";
        rs = st.executeQuery(statement);
        
        ArrayList<Weapon> equipment = new ArrayList();
        while (rs.next()) {
            Weapon w = new Weapon();
            w.setWeaponName(rs.getString("weaponName"));
            w.setShortRange(rs.getString("shortRange"));
            w.setLongRange(rs.getString("longRange"));
            w.setHitShort(rs.getString("hitShort"));
            w.setHitLong(rs.getString("hitLong"));
            w.setStrength(rs.getString("strength"));
            w.setDamage(rs.getString("damage"));
            w.setSaveMod(rs.getString("saveMod"));
            w.setAmmoRoll(rs.getString("ammoRoll"));
            equipment.add(w);
        }
        
        rs.close();
        st.close();
        return equipment;
    }
    
    /**
     * Returns an ArrayList of the Gang's territories and income values.
     * @param gangId The gang whose territories we want
     * @return An ArrayList of Sting[] array in the format {"territoryName", "income"}
     * @throws SQLException 
     */
    public ArrayList<Territory> getGangTerritory(int gangId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT t.territoryName, t.income FROM territories t "
                + "INNER JOIN income i ON t.territoryId = i.territoryId "
                + "WHERE i.gangId = " + gangId + ";";
        rs = st.executeQuery(statement);
        
        ArrayList<Territory> territories = new ArrayList();
        
        while (rs.next()) {
            Territory t = new Territory();
            t.setTerritoryName(rs.getString("territoryName"));
            t.setIncome(rs.getString("income"));
            territories.add(t);
        }
        
        rs.close();
        st.close();
        return territories;
    }
    
    public int calcGangerCost(Ganger g) throws SQLException {
        int cost = 0;
        //base cost for recruiting
        switch (g.getGangerType()) {
            case 'L':
                cost += 120;
                break;
            case 'H':
                cost += 60;
                break;
            case 'G':
                cost += 50;
                break;
            case 'J':
                cost += 25;
                break;
        }
        
        //equipment cost
        st = con.createStatement();
        String statement = "SELECT sum(cost) FROM equipment WHERE "
                + "gangerId = " + g.getGangerId() + ";";
        rs = st.executeQuery(statement);
        
        while (rs.next()) {
            cost += rs.getInt("sum(cost)");
        }
        
        rs.close();
        st.close();
        return cost;
    }
    
    public void increaseCredits(int income, int gangId) throws SQLException {
        st = con.createStatement();
        String statement = "UPDATE gangs SET credits = (SELECT (credits + " + income +") "
                + "AS credits) WHERE gangId = " + gangId + ";";
        st.executeUpdate(statement);
        st.close();
    }
    
    /**
     * Returns a string of injuries formatted in HTML span tags, for index.jsp
     * @param gangerId The ganger's id number.
     * @return A String of HTML span tags where the hover text is the injury text.
     * @throws SQLException 
     */
    public String injuriesList(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT i.injuryName, i.injuryText FROM gangers g "
                + "INNER JOIN gangerInjuries p ON g.gangerId = p.gangerId "
                + "INNER JOIN injuries i on i.injuryId = p.injuryId "
                + "WHERE g.gangerId = " + gangerId + ";";
        rs = st.executeQuery(statement);
        
        String output = "";
        while (rs.next()) {
            output += "<span title='" + rs.getString("injuryText") +"'>"
                    + rs.getString("injuryName") + "</span>&nbsp;";
        }
        
        rs.close();
        st.close();
        return output;
    }
    
    public void addInjury(int gangerId, int injuryId) throws SQLException {
        st = con.createStatement();
        String statement = "INSERT INTO gangerInjuries(gangerId, injuryId) "
                + "VALUES (" + gangerId + ", " + injuryId + ");";
        st.executeUpdate(statement);
        
        st.close();
    }
    
    public void removeAllInjuries(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "DELETE FROM gangerInjuries WHERE "
                + "gangerId = " + gangerId + ";";
        st.executeUpdate(statement);
        
        st.close();
    }
    
    public String getGangerInjuries(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT i.injuryId, i.injuryName "
                + "FROM injuries i WHERE i.injuryId IN "
                + "(SELECT injuryId FROM gangerInjuries WHERE gangerId = " + gangerId + ");";
        rs = st.executeQuery(statement);
        
        String output = "";
        
        while (rs.next()) {
            output += "<option value='" + rs.getString("injuryId") + "' selected>" + rs.getString("injuryName") + "</option>";
        }
        
        rs.close();
        st.close();
        
        return output;
    }
    
    public String getOtherInjuries(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT i.injuryId, i.injuryName "
                + "FROM injuries i WHERE i.injuryId NOT IN "
                + "(SELECT injuryId FROM gangerInjuries WHERE gangerId = " + gangerId + ");";
        rs = st.executeQuery(statement);
        
        String output = "";
        
        while (rs.next()) {
            output += "<option value='" + rs.getString("injuryId") + "'>" + rs.getString("injuryName") + "</option>";
        }
        
        rs.close();
        st.close();
        
        return output;
    }
    
    public String skillsList(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT s.skillName, s.skillText FROM gangers g "
                + "INNER JOIN gangerSkills p ON g.gangerId = p.gangerId "
                + "INNER JOIN skills s on s.skillId = p.skillId "
                + "WHERE g.gangerId = " + gangerId + ";";
        rs = st.executeQuery(statement);
        
        String output = "";
        while (rs.next()) {
            output += "<span title='" + rs.getString("skillText") +"'>"
                    + rs.getString("skillName") + "</span>&nbsp;";
        }
        
        rs.close();
        st.close();
        return output;
    }
    
    public String getGangerSkills(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT s.skillId, s.skillName "
                + "FROM skills s WHERE s.skillId IN "
                + "(SELECT skillId FROM gangerSkills WHERE gangerId = " + gangerId + ");";
        rs = st.executeQuery(statement);
        
        String output = "";
        
        while (rs.next()) {
            output += "<option value='" + rs.getString("skillId") + "' selected>" + rs.getString("skillName") + "</option>";
        }
        
        rs.close();
        st.close();
        
        return output;
    }
    
    public String getOtherSkills(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "SELECT s.skillId, s.skillName "
                + "FROM skills s WHERE s.skillId NOT IN "
                + "(SELECT skillId FROM gangerSkills WHERE gangerId = " + gangerId + ");";
        rs = st.executeQuery(statement);
        
        String output = "";
        
        while (rs.next()) {
            output += "<option value='" + rs.getString("skillId") + "'>" + rs.getString("skillName") + "</option>";
        }
    
        rs.close();
        st.close();
        
        return output;
    }
    
    public void addSkill(int gangerId, int skillId) throws SQLException {
        st = con.createStatement();
        String statement = "INSERT INTO gangerSkills(gangerId, skillId) "
                + "VALUES (" + gangerId + ", " + skillId + ");";
        st.executeUpdate(statement);
        
        st.close();
    }
    
    public void removeAllSkills(int gangerId) throws SQLException {
        st = con.createStatement();
        String statement = "DELETE FROM gangerSkills WHERE "
                + "gangerId = " + gangerId + ";";
        st.executeUpdate(statement);
        
        st.close();
    }
    /**
     * Closes the connection to the database.
     * @throws SQLException
     */
    public void closeBean() throws SQLException {
        con.close();
    }
    
}
