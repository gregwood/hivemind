package src;

/**
 * The class that represents a single Gang member (Ganger).
 * @author weirdvector
 */
public class Ganger {
    
    private int gangerId;
    private int gangId;
    private String name;
    private char gangerType;
    private int move;
    private int ws;
    private int bs;
    private int strength;
    private int toughness;
    private int wounds;
    private int initiative;
    private int attack;
    private int leadership;
    private int exp;
    private int cost;
    private String injuries;
    private String skills;
    private String equipment;
    private boolean isDead = false;

    /**
     * Gets the ganger's id, used in the database.
     * @return gangerId as an int.
     */
    public int getGangerId() {
        return gangerId;
    }

    public int getGangId() {
        return gangId;
    }

    public void setGangId(int gangId) {
        this.gangId = gangId;
    }
    
    /**
     * Sets the ganger's id that will be used in the database.
     * @param gangerId 
     */
    public void setGangerId(int gangerId) {
        this.gangerId = gangerId;
    }

    /**
     * Returns the Ganger's name
     * @return Ganger name as String.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the Ganger's name
     * @param name The Ganger's new name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Get the ganger's type (Leader, Heavy, Ganger, Juve)
     * @return The ganger's type as a char.
     */
    public char getGangerType() {
        return gangerType;
    }
    
    /**
     * Set the ganger's type: L, H, G, or J
     * @param gangerType The new ganger's type.
     */
    public void setGangerType(char gangerType) {
        //keep gangerType as an upperCase char.
        if (gangerType == 'L' || gangerType == 'H' || gangerType == 'G' || gangerType == 'J') {
            this.gangerType = gangerType;
        } else if (gangerType == 'l' || gangerType == 'h' || gangerType == 'g' || gangerType == 'j') {
            gangerType -= 32; //subtract 32 from the ascii value
            this.gangerType = gangerType;
        }
    }
    
    /**
     * Returns the Ganger's move rate.
     * @return Move rate as int.
     */
    public int getMove() {
        return move;
    }

    /**
     * Sets the Ganger's move rate.
     * @param move The new move rate.
     */
    public void setMove(int move) {
        //max move is 4
        if (move > 4) {
            move = 4;
        } if (move < 1) {
            move = 1;
        }
        this.move = move;
    }

    /**
     * Returns the Ganger's Weapon Skill (WS).
     * @return ws as an int.
     */
    public int getWs() {
        return ws;
    }

    /**
     * Sets the Ganger's Weapon Skill (WS).
     * @param ws The new ws.
     */
    public void setWs(int ws) {
        //max WS is 6
        if (ws > 6) {
            ws = 6;
        } else if (ws < 1) {
            ws = 1;
        }
        this.ws = ws;
    }

    /**
     * Returns the Ganger's Ballistic Skill (BS).
     * @return bs as an int.
     */
    public int getBs() {
        return bs;
    }

    /**
     * Sets the Ganger's Ballistic Skill (BS).
     * @param bs the new bs.
     */
    public void setBs(int bs) {
        //max BS is 6
        if (bs > 6) {
            bs = 6;
        } else if (bs < 1) {
            bs = 1;
        }
        this.bs = bs;
    }

    /**
     * Returns the Ganger's strength.
     * @return Strength as an int.
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Sets the Ganger's strength.
     * @param strength The new strength.
     */
    public void setStrength(int strength) {
        //max strength is 4
        if (strength > 4) {
            strength = 4;
        } else if (strength < 1) {
            strength = 1;
        }
        this.strength = strength;
    }

    /**
     * Returns the Ganger's toughness.
     * @return The toughness as an int.
     */
    public int getToughness() {
        return toughness;
    }

    /**
     * Sets the Ganger's toughness.
     * @param toughness The new toughness attribute.
     */
    public void setToughness(int toughness) {
        //max toughness is 4
        if (toughness > 4) {
            toughness = 4;
        } else if (toughness < 1) {
            toughness = 1;
        }
        this.toughness = toughness;
    }

    /**
     * Returns the Ganger's wounds.
     * @return Wounds as an int.
     */
    public int getWounds() {
        return wounds;
    }

    /**
     * Sets the Ganger's wounds.
     * @param wounds The new number of wounds.
     */
    public void setWounds(int wounds) {
        //max wounds are 3
        if (wounds > 3) {
            wounds = 3;
        } else if (wounds < 1) {
            wounds = 1;
        }
        this.wounds = wounds;
    }

    /**
     * Returns the Ganger's initiative.
     * @return The initiative as an int.
     */
    public int getInitiative() {
        return initiative;
    }

    /**
     * Sets the Ganger's initiative.
     * @param initiative The new initiative attribute.
     */
    public void setInitiative(int initiative) {
        if (initiative > 6) {
            initiative = 6;
        } else if (initiative < 1) {
            initiative = 1;
        }
        this.initiative = initiative;
    }

    /**
     * Returns the Ganger's number of attacks.
     * @return attacks as an int.
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Sets the Ganger's number of attacks.
     * @param attack The new attack attribute.
     */
    public void setAttack(int attack) {
        //max attacks is 3
        if (attack > 3) {
            attack = 3;
        } else if (attack < 1) {
            attack = 1;
        }
        this.attack = attack;
    }

    /**
     * Returns the Ganger's Leadership attribute.
     * @return Leadership as an int.
     */
    public int getLeadership() {
        return leadership;
    }

    /**
     * Sets the Ganger's leadership attribute.
     * @param leadership
     */
    public void setLeadership(int leadership) {
        //max leadership is 9
        if (leadership > 9) {
            leadership = 9;
        } else if (leadership < 1) {
            leadership = 1;
        }
        this.leadership = leadership;
    }

    /**
     * Returns the Ganger's experience.
     * @return exp as an int.
     */
    public int getExp() {
        return exp;
    }

    /**
     * Sets the Ganger's experience.
     * @param exp The new experience value.
     */
    public void setExp(int exp) {
        this.exp = exp;
    }

    /**
     * Returns the Ganger's total cost.
     * @return Cost as an int.
     */
    public int getCost() {
        return cost;
    }

    /**
     * Sets the Ganger's cost.
     * @param cost The new cost attribute.
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * Returns the Ganger's equipment.
     * @return equipment as a String.
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     * Sets the Ganger's equipment.
     * @param equipment The new equipment String.
     */
    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }
    
    /**
     * Returns the ganger's injuries
     * @return injuries The injuries the ganger has sustained.
     */
    public String getInjuries() {
        return injuries;
    }

    /**
     * Set the ganger's injuries
     * @param injuries 
     */
    public void setInjuries(String injuries) {
        this.injuries = injuries;
    }

    /**
     * Gets the ganger's special skills.
     * @return String containing the skills.
     */
    public String getSkills() {
        return skills;
    }

    /**
     * Gets the ganger's skills.
     * @param skills The string representing the ganger's skills.
     */
    public void setSkills(String skills) {
        this.skills = skills;
    }
    
    public String toString() {
        String gangerInfo = getName();
        gangerInfo += "\tM:" + getMove();
        gangerInfo += "\tWS:" + getWs();
        gangerInfo += "\tBS:" + getBs();
        gangerInfo += "\tS:" + getStrength();
        gangerInfo += "\tT:" + getToughness();
        gangerInfo += "\tW:" + getWounds();
        gangerInfo += "\tI:" + getInitiative();
        gangerInfo += "\tA:" + getAttack();
        gangerInfo += "\tLd:" + getLeadership();
        gangerInfo += "\tCost:" + getCost();
        gangerInfo += "\tExp:" + getExp();
        gangerInfo += "\nEquipment: " + getEquipment();
        gangerInfo += "\nSkills: " + getSkills();
        gangerInfo += "\nInjuries: " + getInjuries();
        return gangerInfo;
    }
    
    /**
     * Set whether or not the ganger is dead.
     * @param dead True if ganger is dead.
     */
    public void setDead(boolean dead) {
        this.isDead = dead;
    }
    
    /**
     * Returns whether or not the fighter is dead.
     * @return True if dead.
     */
    public boolean isDead() {
        return isDead;
    }
}
