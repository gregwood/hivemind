package src;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author weirdvector
 */
public class Income {
    
    private Random r;
    private String report;
    private int income;
    private int deductions;
    private int totalIncome;
    
    public String getReport(Gang gang, ArrayList<Territory> territories) {
        
        report = "";
        
        for (Territory territory : territories) {
            income = calculateIncome(territory.getIncome());
            totalIncome += income;
            report += "<div><strong>" + territory.getTerritoryName() + "</strong> has produced "
                    + "<strong>" + income + "</strong> credits.<br></div>";
        }
        
        deductions = deductions(gang, totalIncome);
        report += "<div>Deductions: <strong>" + deductions + "</strong> credits.<br></div>";
        
        totalIncome -= deductions;
        
        //Update the database
        try {
            GangBean gangBean = new GangBean();
            gangBean.increaseCredits(totalIncome, gang.getGangId());
            gangBean.closeBean();
        } catch (ClassNotFoundException e) {
        } catch (IllegalAccessException e) {
        } catch (SQLException e) {
        } catch (InstantiationException e) {
        }
        
        report += "<div>Total income: <strong>" + totalIncome + "</strong> credits<br></div>";
        
        return report;
    }
    
    private int deductions(Gang gang, int totalIncome) {
        int size = gang.getGangArrayList().size();
        int deductions = 0;
        int col = 0;
        int row = 0;
        int[][] deductionTable = {
            { 15,  10,   5,   0,   0,   0,  0},
            { 25,  20,  15,   5,   0,   0,  0},
            { 35,  30,  25,  15,   5,   0,  0},
            { 50,  45,  40,  30,  20,   5,  0},
            { 65,  60,  55,  45,  35,  15,  0},
            { 85,  80,  75,  65,  55,  35, 15},
            {105, 100,  95,  85,  75,  55, 35},
            {120, 115, 110, 100,  90,  65, 45},
            {135, 130, 125, 115, 105,  80, 55},
            {145, 140, 135, 125, 115,  90, 65},
            {155, 150, 145, 135, 125, 100, 70}
        };
        
        //get column
        switch (size) {
            case 1:
            case 2:
            case 3:
                col = 0;
                break;
            case 4:
            case 5: 
            case 6:
                col = 1;
                break;
            case 7:
            case 8:
            case 9:
                col = 2;
                break;
            case 10:
            case 11:
            case 12:
                col = 3;
                break;
            case 13: 
            case 14:
            case 15:
                col = 4;
                break;
            case 16:
            case 17:
            case 18:
                col = 5;
                break;
            case 19:
            case 20:
            case 21:
                col = 6;
                break;
        }
        
        //get row
        if (totalIncome <= 29) {
            row = 0;
        } else if (totalIncome <= 49) {
            row = 1;
        } else if (totalIncome <= 79) {
            row = 2;
        } else if (totalIncome <= 119) {
            row = 3;
        } else if (totalIncome <= 169) {
            row = 4;
        } else if (totalIncome <= 229) {
            row = 5;
        } else if (totalIncome <= 299) {
            row = 6;
        } else if (totalIncome <= 379) {
            row = 7;
        } else if (totalIncome <= 459) {
            row = 8;
        } else if (totalIncome <= 559) {
            row = 9;
        } else if (totalIncome <= 669) {
            row = 10;
        }
        
        return deductionTable[row][col];
    }
    
    private int calculateIncome(String income) {
        
        int inc = 0;
        int multiplier = 0;
        r = new Random();
        
        //income will be in the form D6*10, or 10, or 2D6
        String[] calc = income.split("\\*");
        if (calc.length == 1) {
            if (calc[0].charAt(0) == '2') { //must be 2D6
                inc = r.nextInt(6) + 1;
                inc += r.nextInt(6) + 1;
            } else {
                //not a die, must be a number
                try {
                    inc = Integer.parseInt(calc[0]);
                } catch (NumberFormatException e) {
                    //laziness
                    inc = 10;
                }
            }
        } else if (calc.length == 2) {
            if (calc[0].charAt(0) == '2') { //2D6 * __
                multiplier = r.nextInt(6) + 1;
                multiplier += r.nextInt(6) + 1;
                
                try {
                    inc = Integer.parseInt(calc[1]);
                } catch (NumberFormatException e) {
                    inc = 10;
                }
                inc *= multiplier;
            } else if (calc[0].charAt(0) == 'D') { //D6 * __
                multiplier = r.nextInt(6) + 1;
                
                try {
                    inc = Integer.parseInt(calc[1]);
                } catch (NumberFormatException e) {
                    inc = 10;
                }
                inc *= multiplier;
            }
        }
        
        return inc;
    }
}
