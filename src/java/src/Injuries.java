package src;

import java.sql.SQLException;
import java.util.Random;

public class Injuries {
    
    String report;
    Random r;
    int roll;
    private GangBean gangBean;
    
    public Ganger findInjury(Ganger ganger, String gangHouse) 
            throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        
        gangBean = new GangBean();
        r = new Random();
        report = null;
        roll = ((r.nextInt(6) + 1) * 10) + r.nextInt(6) + 1;
        switch (roll) {
            case 11: 
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                report = "<br><strong>" + ganger.getName() + "</strong> has died!<br>The fighter is killed in "
                        + "action and his body abandoned to the mutant rats of the "
                        + "Underhive. All the weapons and equipment carried by the "
                        + "fighter are lost.";
                //TODO: add ganger name to ganger graveyard
                ganger.setDead(true);
                gangBean.addInjury(ganger.getGangerId(), 1);
                break;
            case 21:
                report = "<br><strong>" + ganger.getName() + "</strong> has Multiple Injuries.";
                //TODO: multiple injuries requires rerolls, with recovery and death
                //not being options.
                break;
            case 22: 
                if (!ganger.getInjuries().contains("Chest Wound, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has a Chest Wound.<br>The fighter has "
                            + "been badly wounded in the chest. He recovers but is "
                            + "weakened by the injury and his Toughness characteristic "
                            + "is reduced by -1.";
                    ganger.setToughness(ganger.getToughness() - 1);
                    ganger.setInjuries(ganger.getInjuries()+ "Chest Wound, ");
                    gangBean.addInjury(ganger.getGangerId(), 2);
                }
                break;
            case 23:
                if (!ganger.getInjuries().contains("Leg Wound, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has a Leg Wound.<br>The fighter has "
                            + "smashed a leg. He recovers from his injuries but he "
                            + "can no longer move quickly. The fighter’s Movement "
                            + "characteristic is reduced by -1.";
                    ganger.setMove(ganger.getMove() - 1);
                    ganger.setInjuries(ganger.getInjuries() + "Leg Wound, ");
                    gangBean.addInjury(ganger.getGangerId(), 3);
                }
                break;
            case 24:
                if (!ganger.getInjuries().contains("Arm Wound, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has an Arm Wound.<br>The fighter has "
                            + "smashed one arm. Although he recovers from his injury "
                            + "his strength is permanently reduced as a result. The "
                            + "fighter’s Strength characteristic is reduced by -1 when "
                            + "using that arm. Randomly determine which arm has been "
                            + "hit. Bear in mind that some hand-to-hand weapons use "
                            + "the fighter’s own Strength, eg swords.";
                    ganger.setStrength(ganger.getStrength() - 1);
                    ganger.setInjuries(ganger.getInjuries()+ "Arm Wound, ");
                    gangBean.addInjury(ganger.getGangerId(), 4);
                }
                break;
            case 25:
                if (!ganger.getInjuries().contains("Head Wound, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has a Head Wound.<br>A serious head "
                            + "injury leaves the fighter somewhat unhinged. At the "
                            + "start of each game roll a D6 to determine how he is "
                            + "affected. On a 1-3 the fighter is dazed and confused "
                            + "– he is affected by the rules for stupidity. On a "
                            + "roll of 4-6 the fighter is enraged and uncontrollable "
                            + "– he is affected by the rules for frenzy.";
                    ganger.setInjuries(ganger.getInjuries() + "Head Wound, ");
                    gangBean.addInjury(ganger.getGangerId(), 5);
                }
                break;
            case 26:
                if (ganger.getInjuries().contains("Blinded in One Eye")) {
                    //Fighter is totally blind!
                    report = "<br><strong>" + ganger.getName() + "</strong> has been Blinded in his other eye!<br>"
                            + "Now that " + "<br><strong>" + ganger.getName() + "</strong> is totally blind, "
                            + "he is useless to the gang and is forced into retirement. "
                            + "Your gang unceremoniously gives him the heave-ho, without "
                            + "giving him so much as a white cane, leaving him to "
                            + "fend off the horrors of the Underhive by sense of "
                            + "smell alone.";
                    //TODO: add ganger to ganger GraveYard
                    ganger.setDead(true);
                    gangBean.addInjury(ganger.getGangerId(), 1);
                } else {
                    report = "<br><strong>" + ganger.getName() + "</strong> has been Blinded in One Eye.<br>The "
                            + "fighter survives but loses the sight of one eye. Randomly "
                            + "determine which eye. A character with only one eye has "
                            + "his Ballistic Skill reduced by -1. If the fighter is "
                            + "subsequently blinded in his remaining good eye then he "
                            + "must retire from the gang.";
                    ganger.setBs(ganger.getBs() - 1);
                    ganger.setInjuries(ganger.getInjuries() + "Blind in One Eye, ");
                    gangBean.addInjury(ganger.getGangerId(), 6);
                }
                break;
            case 31:
                if (ganger.getInjuries().contains("Partially Deafened")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has been Partially Deafened again!<br>"
                            + "The fighter survives but is partially deafened as a "
                            + "result of his injuries. His Leadership is decreased "
                            + "because nobody in the gang has the patience to "
                            + "repeat things to him.";
                    ganger.setLeadership(ganger.getLeadership() - 1);
                    gangBean.addInjury(ganger.getGangerId(), 7);
                } else {
                    report = "<br><strong>" + ganger.getName() + "</strong> is Partially Deafened.<br>The fighter "
                            + "survives but is partially deafened as a result of his "
                            + "injuries. An individual suffers no penalty if he is "
                            + "partially deafened, but if he is deafened for a second "
                            + "time he suffers -1 from his Leadership characteristic.";
                    ganger.setInjuries(ganger.getInjuries()+ "Partially Deafened, ");
                    gangBean.addInjury(ganger.getGangerId(), 7);
                }
                break;
            case 32: 
                if (!ganger.getInjuries().contains("Shell Shocked, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> is Shell Shocked.<br>The fighter survives "
                            + "but is extremely nervous and jumpy as a result of the "
                            + "traumatic injuries he has suffered. His Initiative "
                            + "characteristic is reduced by -1.";
                    ganger.setInitiative(ganger.getInitiative() - 1);
                    ganger.setInjuries(ganger.getInjuries()+ "Shell Shocked, ");
                    gangBean.addInjury(ganger.getGangerId(), 8);
                }
                break;
            case 33:
                int fingers = r.nextInt(3) + 1;
                report = "<br><strong>" + ganger.getName() + "</strong> has lost " + fingers + " fingers.<br>"
                        + "Wounds to a hand result in the loss of D3 fingers. "
                        + "Randomly determine which hand is affected. The fighter’s"
                        + " Weapon Skill is reduced by -1. If a fighter loses all "
                        + "five fingers on a hand then he may no longer use that "
                        + "hand: he may not carry anything in it, and is unable to "
                        + "use weapons that require two hands.";
                ganger.setWs(ganger.getWs() - 1);
                ganger.setInjuries(ganger.getInjuries()+ "Hand Injury (missing "
                        + fingers + " fingers), ");
                gangBean.addInjury(ganger.getGangerId(), 9);
                break;
            case 34:
            case 35:
            case 36:
                if (!ganger.getInjuries().contains("Old Battle Wound, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has an Old Battle Wound.<br>The fighter "
                            + "recovers but his old wound sometimes affects his health. "
                            + "Roll a D6 before each game. On the roll of a 1 the "
                            + "fighter’s old wound is playing up and he is unable to "
                            + "take part in the forthcoming battle.";
                    ganger.setInjuries(ganger.getInjuries()+ "Old Battle Wound, ");
                    gangBean.addInjury(ganger.getGangerId(), 10);
                }
                break;
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
                report = "<br><strong>" + ganger.getName() + "</strong> has made a full recovery.";
                break;
            case 56:
                if (!ganger.getInjuries().contains("Bitter Enmity, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has Bitter Enmity.<br>Although he makes "
                            + "a full physical recovery, the fighter has been "
                            + "psychologically scarred by his experiences. He develops "
                            + "a bitter enmity for the gang that was responsible for his injury.";
                    ganger.setInjuries(ganger.getInjuries()+ "Bitter Enmity, ");
                    gangBean.addInjury(ganger.getGangerId(), 11);
                }
                break;
            case 61:
            case 62:
            case 63:
                report = "<br><strong>" + ganger.getName() + "</strong> has been captured!<br>The fighter is captured. Captives may be exchanged,\n" +
                        "ransomed back or sold into slavery. If both gangs hold " +
                        "captives then they must be exchanged on a one-for-one " +
                        "basis, starting with models of the highest value. Any " +
                        "remaining captives must be ransomed back to their own " +
                        "gang if the player is willing to pay the captor’s asking " +
                        "price. There is no fixed value for ransom – it is a matter " +
                        "for the players to decide for themselves. Finally, fighters " +
                        "who are neither exchanged or ransomed may be sold to " +
                        "the Guilders as slaves earning the captor D6 x 5 Guilder " +
                        "credits. Captives who are exchanged or ransomed retain " +
                        "all of their weapons and equipment; if captives are sold " +
                        "their weaponry and equipment is kept by the captors.";
                gangBean.addInjury(ganger.getGangerId(), 12);
                break;
            case 64:
                if (!ganger.getInjuries().contains("Horrible Scars, ")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has Horrible Scars.<br>The fighter "
                            + "recovers from his injuries but is left horribly disfigured. "
                            + "His scarred and distorted features inspire fear as "
                            + "described in the Advanced Rules section of the rulebook.";
                    ganger.setInjuries(ganger.getInjuries() + "Horrible Scars, ");
                    gangBean.addInjury(ganger.getGangerId(), 13);
                }
                break;
            case 65:
                if (ganger.getInjuries().contains("Impressive Scars")) {
                    report = "<br><strong>" + ganger.getName() + "</strong> has Impressive Scars, again!<br> "
                            + "He was already impressively scarred, so there is "
                            + "no additional bonus. He is much uglier, however, "
                            + "which is kind of cool in its own way.";
                } else {
                    report = "<br><strong>" + ganger.getName() + "</strong> has Impressive Scars.<br>The fighter "
                            + "recovers and is left with impressive scars as testament "
                            + "to his bravery. Add +1 to the fighter’s Leadership "
                            + "characteristic. This bonus applies only once, further "
                            + "impressive scars have no additional effect.";
                    ganger.setInjuries(ganger.getInjuries()+ "Impressive Scars, ");
                    ganger.setLeadership(ganger.getLeadership() + 1);
                    gangBean.addInjury(ganger.getGangerId(), 14);
                }
                break;
            case 66:
                report = "<br><strong>" + ganger.getName() + "</strong> has survived against all odds.<br>"
                        + "The fighter regains consciousness alone in the darkness, " +
                        "given up for dead by his companions and overlooked by " +
                        "his enemies. Despite his injuries he makes his way back " +
                        "home. He recovers fully and his uncanny survival earns " +
                        "him an additional D6 Experience points.";
                int exp = r.nextInt(6) + 1;
                Experience experience = new Experience();
                ganger = experience.levelUp(ganger, ganger.getExp(), exp, gangHouse);
        }
        return ganger;
    }
    
    public String getReport() {
        return report;
    }
}
