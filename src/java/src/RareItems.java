
package src;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author weirdvector
 */
public class RareItems {
    
    public ArrayList<Weapon> getRareItems() throws ClassNotFoundException, 
            IllegalAccessException, SQLException, InstantiationException {
        
        Date d = new Date();
        //get a random seed every 10 minutes
        Random rand = new Random((d.getMinutes() / 10) * d.getHours() * d.getDate() * d.getMonth() * d.getYear());
        
        GangBean gangBean = new GangBean();
        ArrayList<Weapon> allRares = gangBean.getRareEquipment();
        
        //D3 rare weapons are returned
        int num = rand.nextInt(3) + 1;
        ArrayList<Weapon> todaysWares = new ArrayList(num);

        for (int i = 0; i < num; i++) {
            Weapon w = allRares.get(rand.nextInt(allRares.size()));
            w.setCost(calcRareCost(w.getCost()));
            todaysWares.add(w);
        }
        return todaysWares;
    }
    
    public String calcRareCost(String s) {
        Date d = new Date();
        Random rand = new Random((d.getMinutes() / 10) * d.getHours() * d.getDate() * d.getMonth() * d.getYear());
        
        int finalPrice = 100; //dummy value
        
        String[] nums = s.split("\\+");
        if (nums.length > 1) {
            int numOfDice = 1;
            try {
                numOfDice = Integer.parseInt(nums[1].substring(0,1));
            } catch (NumberFormatException e) {
                //format was just D6, no number
                //number of dice is already 1, leave it
            }
            finalPrice = Integer.parseInt(nums[0]) + numOfDice * (rand.nextInt(6) + 1);
        } else {
            //maybe multiplication, not addition.
            nums = s.split("\\*");
            
            if (nums.length > 1) {
                //the only items that use multiplication are times D6
                finalPrice = Integer.parseInt(nums[0]) * (rand.nextInt(6) + 1);
            } else {
                //the price was just one number to start with
                finalPrice = Integer.parseInt(s);
            }
        }
        return Integer.toString(finalPrice);
    }
}
