package src;

import java.sql.SQLException;
import java.util.Random;

/**
 *
 * @author weirdvector
 */
public class Skills {
    
    Random r;
    String report;
    GangBean gangBean;
    
    public Ganger randomAgilitySkill(Ganger ganger) 
            throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        gangBean = new GangBean();
        r = new Random();
        int roll = r.nextInt(6) + 1;
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Catfall.")) {
                    randomAgilitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Catfall.");
                report = "Catfall: A model with the Catfall skill halves "
                        + "the distance fallen when calculating the strength of "
                        + "any hits which result from falling. Round fractions "
                        + "down.";
                gangBean.addSkill(ganger.getGangerId(), 1);
                break;
            case 2:
                if (ganger.getSkills().contains(" Dodge.")) {
                    randomAgilitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Dodge.");
                report = "Dodge: A model with this skill receives a 6+ "
                        + "saving throw against hits from shooting or in "
                        + "hand-to-hand combat.This is an unmodified save – ie, "
                        + "it is not affected by a weapon’s armour save modifier. "
                        + "The save is taken separately and in addition to any "
                        + "saves for armour.  If a model successfully dodges from "
                        + "a weapon which uses a template or blast marker then "
                        + "move the model up to 2\". As long as his move gets "
                        + "him outside the template area the fighter avoids the "
                        + "hit. Otherwise he may still be hit, even though he "
                        + "has dodged successfully.";
                gangBean.addSkill(ganger.getGangerId(), 2);
                break;
            case 3: 
                if (ganger.getSkills().contains(" Jump Back.")) {
                    randomAgilitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Jump Back.");
                report = "At the start of any hand-to-hand combat phase "
                        + "the model may attempt to disengage from combat by "
                        + "jumping back. Roll a D6. If the score is less than "
                        + "the model’s Initiative it may immediately jump back 2\" "
                        + "leaving any hand-to-hand opponents behind. If the "
                        + "score is equal to or greater than the model’s Initiative "
                        + "it must remain and continue to fight as normal.";
                gangBean.addSkill(ganger.getGangerId(), 3);
                break;
            case 4:
                if (ganger.getSkills().contains(" Leap.")) {
                    randomAgilitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Leap.");
                report = "The model may leap D6 inches during the movement "
                        + "phase in addition to his normal movement. He may "
                        + "move and leap, run and leap or charge and leap, but "
                        + "he can only leap once during the turn. A leap will "
                        + "take the model over any man-high obstacle without "
                        + "penalty, including enemy models. In addition, the "
                        + "model can leap out of hand-to-hand combat at the end"
                        + " of any hand-to-hand combat phase without suffering"
                        + " any penalty whatsoever. The leap may be used to "
                        + "jump across gaps, but in this case the player must "
                        + "commit the model to making the leap before rolling "
                        + "the dice to see how far the model jumps. If the model "
                        + "fails to make it all the way across, then it falls "
                        + "through the gap.";
                gangBean.addSkill(ganger.getGangerId(), 4);
                break;
            case 5:
                if (ganger.getSkills().contains(" Quick Draw.")) {
                    randomAgilitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Quick Draw.");
                report = "A model with this skill can double its Initiative "
                        + "when it makes a fast draw in a gunfight. See the Gunfight "
                        + "scenario to find how this works.";
                gangBean.addSkill(ganger.getGangerId(), 5);
                break;
            case 6:
                if (ganger.getSkills().contains(" Sprint.")) {
                    randomAgilitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Sprint.");
                report = "The model may triple its movement rate when it "
                        + "runs or charges, rather than doubling it as normal.";
                gangBean.addSkill(ganger.getGangerId(), 6);
                break;
        }
        return ganger;
    }
    
    public Ganger randomCombatSkill(Ganger ganger) 
            throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        r = new Random();
        int roll = r.nextInt(6) + 1;
        gangBean = new GangBean();
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Combat Master.")) {
                    randomCombatSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Combat Master.");
                report = "If the model is attacked by multiple opponents in " +
                    "hand-to-hand combat then it can use the enemies’ " +
                    "numbers against them. For each opponent over one, " +
                    "add +1 to the model’s Weapon Skill.";
                gangBean.addSkill(ganger.getGangerId(), 7);
                break;
            case 2:
                if (ganger.getSkills().contains(" Disarm.")) {
                    randomCombatSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Disarm.");
                report = "The model may use this skill against one close combat " +
                    "opponent at the start of the hand-to-hand combat " +
                    "phase. Roll a D6. On a roll of 4+ the enemy " +
                    "automatically loses one weapon of your choice. This " +
                    "weapon is destroyed and can no longer be used – it is " +
                    "deleted permanently from the gang roster. A model is " +
                    "always assumed to have a knife, even if he has been " +
                    "disarmed of all his other weapons.";
                gangBean.addSkill(ganger.getGangerId(), 8);
                break;
            case 3:
                if (ganger.getSkills().contains(" Feint.")) {
                    randomCombatSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Feint.");
                report = "The model may ‘convert’ any parries it is allowed to " +
                    "use into extra attacks at +1 A per parry. The attack is " +
                    "used instead of the parry. The model may choose to " +
                    "feint or parry each time it attacks (eg, you could parry " +
                    "one time and feint the next).";
                gangBean.addSkill(ganger.getGangerId(), 9);
                break;
            case 4:
                if (ganger.getSkills().contains(" Parry.")) {
                    randomCombatSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Parry.");
                report = "A model with the Parry skill may parry in hand-to-" +
                    "hand combat even if he does not have a sword or " +
                    "another weapon suitable for parrying. The model " +
                    "knocks aside blows using the flats of his hands or the " +
                    "haft of his weapon. If the model has a weapon that " +
                    "may parry, it may force an opponent to re-roll up to 2 " +
                    "Attack dice when parrying, rather than just 1.";
                gangBean.addSkill(ganger.getGangerId(), 10);
                break;
            case 5:
                if (ganger.getSkills().contains(" Counter Attack.")) {
                    randomCombatSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Counter Attack.");
                report = "If a model carries a sword he is normally able to parry " +
                    "(force his opponent to re-roll his best Attack dice). " +
                    "However, a parry is cancelled out if the opponent is " +
                    "also armed with a sword or has the Parry skill as " +
                    "described above. If a fighter has the Counter Attack " +
                    "skill and his parry is cancelled for whatever reason, " +
                    "then he may make a Counter Attack – roll an extra " +
                    "Attack dice immediately.";
                gangBean.addSkill(ganger.getGangerId(), 11);
                break;
            case 6:
                if (ganger.getSkills().contains(" Step Aside.")) {
                    randomCombatSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Step Aside.");
                report = "The model has an uncanny ability to step aside and " +
                    "dodge blows in hand-to-hand combat. If the model is " +
                    "hit in hand-to-hand fighting roll a D6. On a roll of 4+ " +
                    "the model steps out of the way of the blow and is " +
                    "unharmed.";
                gangBean.addSkill(ganger.getGangerId(), 12);
                break;
        }
        return ganger;
    }
    
    public Ganger randomStealthSkill(Ganger ganger) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        r = new Random();
        int roll = r.nextInt(6) + 1;
        gangBean = new GangBean();
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Ambush.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Ambush.");
                report = "The model is allowed to go into overwatch and hide " +
                    "in the same turn. Normally a fighter must expend his " +
                    "entire turn to go into overwatch, but a fighter with the " +
                    "Ambush skill may do both.";
                gangBean.addSkill(ganger.getGangerId(), 13);
                break;
            case 2:
                if (ganger.getSkills().contains(" Dive.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Dive.");
                report = "A model with this skill can run and hide in the same " +
                    "turn. Normally a fighter who runs cannot hide in the " +
                    "same turn, but a fighter with Dive skill can run and " +
                    "hide.";
                gangBean.addSkill(ganger.getGangerId(), 14);
                break;
            case 3:
                if (ganger.getSkills().contains(" Escape Artist.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Escape Artist.");
                report = "This model may never be captured after a battle. If " +
                    "you roll a ‘Captured’ result on the Serious Injuries " +
                    "table then the fighter escapes unharmed together " +
                    "with his equipment.";
                gangBean.addSkill(ganger.getGangerId(), 15);
                break;
            case 4:
                if (ganger.getSkills().contains(" Evade.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Evade.");
                report = "The model ducks and weaves as he moves making " +
                    "him very hard to hit. Any enemy shooting from short " +
                    "range suffers a -2 to hit penalty, while any enemy " +
                    "shooting at long range suffers a -1 penalty. This " +
                    "penalty only applies if the fighter is in the open and " +
                    "not if he is behind cover.";
                gangBean.addSkill(ganger.getGangerId(), 16);
                break;
            case 5:
                if (ganger.getSkills().contains(" Infiltration.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Infiltration.");
                report = "A model with this skill is always placed on the " +
                    "battlefield after the opposing gang and can be placed " +
                    "anywhere on the table as long as it is out of sight of " +
                    "the opposing gang. If both players have models that " +
                    "can infiltrate roll a D6 each, lowest roll sets up first.";
                gangBean.addSkill(ganger.getGangerId(), 17);
                break;
            case 6:
                if (ganger.getSkills().contains(" Sneak Up.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Sneak Up.");
                report = "Any sentry attempting to spot this model must halve " +
                    "his normal spotting distance. Rules for sentries and " +
                    "spotting are covered in the relevant scenarios.";
                gangBean.addSkill(ganger.getGangerId(), 18);
                break;
        }
        return ganger;
    }
    
    public Ganger randomMuscleSkill(Ganger ganger) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        r = new Random();
        int roll = r.nextInt(6) + 1;
        gangBean = new GangBean();
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Body Slam.")) {
                    randomMuscleSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Body Slam.");
                report = "The model adds +2 to its WS in the turn when it " +
                    "charges instead of only +1.";
                gangBean.addSkill(ganger.getGangerId(), 19);
                break;
            case 2:
                if (ganger.getSkills().contains(" Bulging Biceps.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Bulging Biceps.");
                report = "This skill may only be taken by a heavy. The heavy is " +
                    "allowed to move and shoot with weapons that would " +
                    "normally restrict the model to either moving or " +
                    "shooting. However, if the model moves and shoots in " +
                    "the same turn it suffers a -1 to hit penalty.";
                gangBean.addSkill(ganger.getGangerId(), 20);
                break;
            case 3:
                if (ganger.getSkills().contains(" Crushing Blow.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Crushing Blow.");
                report = "A model with Crushing Blow skill has a +1 Strength " +
                    "characteristic bonus in hand-to-hand combat. As a " +
                    "fighter’s own Strength is used as the basis for " +
                    "calculating the strengths of hand-to-hand weapons " +
                    "the bonus will apply to all such weapons.";
                gangBean.addSkill(ganger.getGangerId(), 21);
                break;
            case 4:
                if (ganger.getSkills().contains(" Head Butt.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Head Butt.");
                report = "If the model inflicts 2 or more hits in hand-to-hand " +
                    "combat then he may choose to exchange all hits for a " +
                    "single hit with a further strength bonus. The bonus " +
                    "equals +1 for each extra hit scored, so you could " +
                    "exchange 2 S4 hits for a single S5 hit, or 3 S4 hits for " +
                    "a single S6 hit, and so on.";
                gangBean.addSkill(ganger.getGangerId(), 22);
                break;
            case 5:
                if (ganger.getSkills().contains(" Hurl Opponent.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Hurl Opponent.");
                report = "If you win a round of combat, instead of hitting your " +
                    "opponent you can throw him D6\" in the direction of " +
                    "your choice. The thrown model takes a single hit " +
                    "equal to half the distance rolled. If it hits a solid object " +
                    "(such as a wall) before it reaches the full distance " +
                    "thrown it will stop there. If it hits another model, then " +
                    "both models take a hit equal to half the distance " +
                    "rolled. Note that the best way to use this skill is to " +
                    "throw opposing models off tall buildings!";
                gangBean.addSkill(ganger.getGangerId(), 23);
                break;
            case 6:
                if (ganger.getSkills().contains(" Iron Jaw.")) {
                    randomStealthSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Iron Jaw.");
                report = "If a model with this skill is hit in hand-to-hand combat " +
                    "reduce the strength of each hit suffered by 1 point.";
                gangBean.addSkill(ganger.getGangerId(), 24);
                break;
        }
        return ganger;
    }
    
    public Ganger randomFerocitySkill(Ganger ganger) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        r = new Random();
        int roll = r.nextInt(6) + 1;
        gangBean = new GangBean();
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Berserk Charge.")) {
                    randomFerocitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Berserk Charge.");
                report = "A model with this skill rolls double the number of Attack  " +
                    "dice on its profile in the turn when it charges. However,  " +
                    "a model making a berserk charge may not parry that turn. ";
                gangBean.addSkill(ganger.getGangerId(), 25);
                break;
            case 2:
                if (ganger.getSkills().contains(" Impetuous.")) {
                    randomFerocitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Impetuous.");
                report = "If a model has this skill he may increase the range of his " +
                    "follow-up move in hand-to-hand combat from 2\" to 4\".";
                gangBean.addSkill(ganger.getGangerId(), 26);
                break;
            case 3:
                if (ganger.getSkills().contains(" Iron Will.")) {
                    randomFerocitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Iron Will.");
                report = "Only the gang leader may have this skill. It allows you to " +
                    "re-roll a failed Bottle roll as long as the leader is not down " +
                    "or out of action.";
                gangBean.addSkill(ganger.getGangerId(), 27);
                break;
            case 4:
                if (ganger.getSkills().contains(" Killer Reputation.")) {
                    randomFerocitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Killer Reputation.");
                report = "A model with this skill has such a reputation as a vicious " +
                    "and depraved killer that his foes quail when he charges " +
                    "them. This causes fear and the enemy must take a " +
                    "psychology test for fear as appropriate.";
                gangBean.addSkill(ganger.getGangerId(), 28);
                break;
            case 5:
                if (ganger.getSkills().contains(" Nerves of Steel.")) {
                    randomFerocitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() +  " Nerves of Steel.");
                report = "If the model fails a dice roll to avoid being pinned it may " +
                    "make the roll again.";
                gangBean.addSkill(ganger.getGangerId(), 29);
                break;
            case 6:
                if (ganger.getSkills().contains(" True Grit.")) {
                    randomFerocitySkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " True Grit.");
                report = "Treat a roll of 1 or 2 as a flesh wound when rolling for the " +
                    "extent of injuries. A roll of 3-5 indicates the model has " +
                    "gone down, and a roll of 6 means it is out of action, as " +
                    "normal. When using special injury charts (needle guns, " +
                    "for example) add 1 to the lowest result band in the same " +
                    "way as above.";
                gangBean.addSkill(ganger.getGangerId(), 30);
                break;
        }
        return ganger;
    }
    
    public Ganger randomShootingSkill(Ganger ganger) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        r = new Random();
        int roll = r.nextInt(6) + 1;
        gangBean = new GangBean();
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Crack Shot.")) {
                    randomShootingSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Crack Shot.");
                report = "A model with this skill can re-roll the Injury dice when " +
                    "rolling injuries he has inflicted by shooting. You must " +
                    "accept the result of the second roll regardless of the " +
                    "result.";
                gangBean.addSkill(ganger.getGangerId(), 31);
                break;
            case 2:
                if (ganger.getSkills().contains(" Fast Shot.")) {
                    randomShootingSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Fast Shot.");
                report = "If a model has Fast Shot skill he may shoot several " +
                    "times in the shooting phase and not just once as " +
                    "normal. The model can shoot as many times as his " +
                    "Attacks characteristic. He can shoot at the same target " +
                    "or at separate targets as you wish. " +
                    "This skill may only be used with pistols and basic " +
                    "weapons. It may not be used with special or heavy " +
                    "weapons as they are far too cumbersome.";
                gangBean.addSkill(ganger.getGangerId(), 32);
                break;
            case 3:
                if (ganger.getSkills().contains(" Gunfighter.")) {
                    randomShootingSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Gunfighter.");
                report = "The model can aim and fire a pistol from each hand. " +
                    "This enables him to take two shots in the shooting " +
                    "phase if he carries two pistols. If he carries a basic, " +
                    "special or heavy weapon he always requires one hand " +
                    "to hold this and so cannot use two pistols at once.";
                gangBean.addSkill(ganger.getGangerId(), 33);
                break;
            case 4:
                if (ganger.getSkills().contains(" Hip Shooting.")) {
                    randomShootingSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Hip Shooting.");
                report = "The model is allowed to shoot even if it ran in the " +
                    "same turn. However, if it does so it suffers a -1 to hit " +
                    "modifier and cannot count any bonuses from sights. " +
                    "Note that it is impossible to run and shoot with a " +
                    "heavy weapon, even with the Hip Shooting skill.";
                gangBean.addSkill(ganger.getGangerId(), 34);
                break;
            case 5:
                if (ganger.getSkills().contains(" Marskman.")) {
                    randomShootingSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Marksman.");
                report = "A model with the Marksman skill may ignore the " +
                    "normal restriction which obliges fighters to shoot at " +
                    "the nearest target. Instead, he can shoot at any target " +
                    "he can see. " +
                    "In addition, a model with the Marksman skill may " +
                    "shoot at targets at extreme range – this is between " +
                    "normal maximum range and half as far again. For " +
                    "example, a lasgun has a normal maximum range of " +
                    "24\" and an extreme range of 24-36\". Shots at extreme " +
                    "range suffer the same ‘to hit’ penalty as long range. " +
                    "This skill may only be used with basic weapons. It may " +
                    "not be used with pistols, special or heavy weapons.";
                gangBean.addSkill(ganger.getGangerId(), 35);
                break;
            case 6:
                if (ganger.getSkills().contains(" Rapid Fire.")) {
                    randomShootingSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Rapid Fire.");
                report = "If the model does not move in its movement phase it " +
                    "can shoot twice in the shooting phase. This skill only " +
                    "works with one specified kind of pistol or basic " +
                    "weapon which you must choose when the skill is " +
                    "earned. Note this down on the gang roster – eg, Rapid " +
                    "Fire/Bolt pistol.";
                gangBean.addSkill(ganger.getGangerId(), 36);
                break;
        }
        return ganger;
    }
    
    public Ganger randomTechnoSkill(Ganger ganger) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        r = new Random();
        int roll = r.nextInt(6) + 1;
        gangBean = new GangBean();
        
        switch (roll) {
            case 1:
                if (ganger.getSkills().contains(" Armourer.")) {
                    randomTechnoSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Armourer.");
                report = "The armourer checks all the weapons being used by the " +
                    "gang before the battle starts. Any model in the gang may " +
                    "add +1 to any and all Ammo rolls (including a roll to " +
                    "determine if a weapon explodes). A roll of 1 is always a " +
                    "failure regardless.";
                gangBean.addSkill(ganger.getGangerId(), 37);
                break;
            case 2:
                //gangers only
                if (ganger.getGangerType() == 'G') {
                    if (ganger.getSkills().contains(" Fixer.")) {
                        randomTechnoSkill(ganger);
                        break;
                    }
                    ganger.setSkills(ganger.getSkills() + " Fixer.");
                    report = "Gangers only. If the model is used to work a piece of " +
                        "territory with a randomly generated income, you may re- " +
                        "roll the dice if you do not like the first result. You must " +
                        "accept the result of the second roll.";
                    gangBean.addSkill(ganger.getGangerId(), 38);
                } else {
                    ganger = randomTechnoSkill(ganger);
                }
                break;
            case 3:
                if (ganger.getSkills().contains(" Inventor.")) {
                    randomTechnoSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Inventor.");
                report = "Roll a D6 after each battle. On a roll of 6 the model has " +
                    "invented something! Randomly select an item from the " +
                    "rare Trade Chart in the Trading Post section (page 97). " +
                    "Whatever is selected is the item that has been invented.";
                gangBean.addSkill(ganger.getGangerId(), 39);
                break;
            case 4:
                if (ganger.getSkills().contains(" Medic.")) {
                    randomTechnoSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Medic.");
                report = "The model has some experience of patching up his fellow " +
                    "fighters. If your gang includes a fighter with this skill you " +
                    "can re-roll a result on the Serious Injury table for one " +
                    "model after a battle.";
                gangBean.addSkill(ganger.getGangerId(), 40);
                break;
            case 5:
                //only available to gangers or juves
                if (ganger.getGangerType() == 'G' || ganger.getGangerType() == 'J') {
                    if (ganger.getSkills().contains(" Specialist.")) {
                        randomTechnoSkill(ganger);
                        break;
                    }
                    ganger.setSkills(ganger.getSkills() + " Specialist.");
                    report = "This skill may only be taken by juves or gangers. It allows " +
                        "the model to be armed with a special weapon.";
                    gangBean.addSkill(ganger.getGangerId(), 41);
                } else {
                    ganger = randomTechnoSkill(ganger);
                }
                break;
            case 6:
                if (ganger.getSkills().contains(" Weaponsmith.")) {
                    randomTechnoSkill(ganger);
                    break;
                }
                ganger.setSkills(ganger.getSkills() + " Weaponsmith.");
                report = "A model with this skill may ignore failed Ammo rolls and " +
                    "weapon explosions on a D6 roll of 4+.";
                gangBean.addSkill(ganger.getGangerId(), 42);
                break;           
        }
        return ganger;
    
    }
    
    public String getReport() {
        return report;
    }
}
