
package src;

/**
 *
 * @author weirdvector
 */
public class Territory {
    
    private String territoryName;
    private String income;

    public String getTerritoryName() {
        return territoryName;
    }

    public void setTerritoryName(String territoryName) {
        this.territoryName = territoryName;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }
    
}
