
package src;

/**
 *
 * @author weirdvector
 */
public class Weapon {
    
    private int weaponId;
    private String weaponName;
    private String shortRange;
    private String longRange;
    private String hitShort;
    private String hitLong;
    private String strength;
    private String damage;
    private String saveMod;
    private String ammoRoll;
    private String type;
    private String cost;
    private String availability;

    public int getWeaponId() {
        return weaponId;
    }

    public void setWeaponId(int weaponId) {
        this.weaponId = weaponId;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public void setWeaponName(String weaponName) {
        this.weaponName = weaponName;
    }

    public String getShortRange() {
        return shortRange;
    }

    public void setShortRange(String shortRange) {
        this.shortRange = shortRange;
    }

    public String getLongRange() {
        return longRange;
    }

    public void setLongRange(String longRange) {
        this.longRange = longRange;
    }

    public String getHitShort() {
        return hitShort;
    }

    public void setHitShort(String hitShort) {
        this.hitShort = hitShort;
    }

    public String getHitLong() {
        return hitLong;
    }

    public void setHitLong(String hitLong) {
        this.hitLong = hitLong;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public String getSaveMod() {
        return saveMod;
    }

    public void setSaveMod(String saveMod) {
        this.saveMod = saveMod;
    }

    public String getAmmoRoll() {
        return ammoRoll;
    }

    public void setAmmoRoll(String ammoRoll) {
        this.ammoRoll = ammoRoll;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }
    
    
}
