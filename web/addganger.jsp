<%-- 
    Document   : AddGanger
    Created on : 28-Mar-2015, 2:54:50 PM
    Author     : weirdvector
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    int gangId;
    if (request.getParameter("id") == null) {
        gangId = 1;
    } else {
        gangId = Integer.parseInt(request.getParameter("id"));
    }
    ArrayList<Gang> ganglist = gangBean.getGangs();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/hivemind.css">
        <link rel="shortcut icon" href="img/icon.png" />
        <title>Hivemind</title>
    </head>
    <body>
        <div id="container">
            <h1>Admin mode</h1>
            <div id="editPanel">
                <form action="handler/add.jsp" method="post">
                    <table id="editGanger">
                        <tr>
                            <th colspan="10">Add Ganger</th>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td colspan="2"><input type="text" value=""name="gangerName"></td>
                            <td>
                                <select name="type" id="type" onchange="update()">
                                    <option value="J">J</option>
                                    <option value="G">G</option>
                                    <option value="H">H</option>
                                    <option value="L">L</option>
                                </select>
                            </td>
                            <td>Cost:</td>
                            <td><input type="text" value="25" size="2" id="cost" name="cost"></td>
                            <td>Exp:</td>
                            <td><input type="text" value="0" size="2" id="exp" name="experience"></td>
                            <td>ID:</td>
                            <td><input type="text" value="" size="2" name="gangerId" disabled></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>M</td>
                            <td>WS</td>
                            <td>BS</td>
                            <td>S</td>
                            <td>T</td>
                            <td>W</td>
                            <td>I</td>
                            <td>A</td>
                            <td>Ld</td>
                        </tr>
                        <tr>
                            <td>Stats:</td>
                            <td><input type="text" value="4" size="2" maxlength="1" id="m" name="move"></td>
                            <td><input type="text" value="2" size="2" maxlength="1" id="ws" name="ws"></td>
                            <td><input type="text" value="2" size="2" maxlength="1" id="bs" name="bs"></td>
                            <td><input type="text" value="2" size="3" maxlength="1" id="s" name="s"></td>
                            <td><input type="text" value="3" size="2" maxlength="1" id="t" name="t"></td>
                            <td><input type="text" value="1" size="2" maxlength="1" id="w" name="w"></td>
                            <td><input type="text" value="3" size="2" maxlength="1" id="i" name="i"></td>
                            <td><input type="text" value="1" size="2" maxlength="1" id="a" name="a"></td>
                            <td><input type="text" value="6" size="2" maxlength="1" id="ld" name="ld"></td>
                        </tr>
                        <tr>
                            <td>Skills:</td>
                            <td colspan="4"><input type="text" value="" name="skills"></td>
                            <td>Injuries:</td>
                            <td colspan="4"><input type="text" value="" name="injuries"></td>
                        </tr>
                        <tr>
                            <td>Gang</td>
                            <td colspan='3'>
                                <select name="gangId">
                                    <% for (int i = 0; i < ganglist.size(); i++) {
                                            if (ganglist.get(i).getGangId() == gangId) {
                                    %>
                                    <option selected="selected" value="<%= ganglist.get(i).getGangId()%>"><%= ganglist.get(i).getGangName()%></option>
                                    <%  } else {%>
                                    <option value="<%= ganglist.get(i).getGangId()%>"><%= ganglist.get(i).getGangName()%></option>
                                    <%  }
                                        }
                                    %>
                                </select>
                            </td>
                            <td class="buttonCell" colspan="6">
                                <input type='submit' value="Add new ganger">
                            </td>
                        </tr>
                    </table>
                    <table id="weaponList">
                        <tr>
                            <th>Stash</th>
                        </tr>
                        <tr>
                            <td>
                                <select id="equipment" name="stash" size="10" multiple>
                                    <%
                                        ArrayList<Weapon> weapons = gangBean.getStash(gangId);
                                        for (Weapon weapon : weapons) {
                                            out.print("<option value='" + weapon.getWeaponId() + "cost" + weapon.getCost() + "'>" + weapon.getWeaponName() + "</option>");
                                        }
                                    %>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <script>
            //set defaults based on the ganger type selected
            function update() {
                var gangerType = document.getElementById("type").value;
                if (gangerType === "J") {
                    document.getElementById("cost").value = 25;
                    document.getElementById("exp").value = 0;
                    document.getElementById("m").value = 4;
                    document.getElementById("ws").value = 2;
                    document.getElementById("bs").value = 2;
                    document.getElementById("s").value = 3;
                    document.getElementById("t").value = 3;
                    document.getElementById("w").value = 1;
                    document.getElementById("i").value = 3;
                    document.getElementById("a").value = 1;
                    document.getElementById("ld").value = 6;
                } else if (gangerType === "G") {
                    document.getElementById("cost").value = 50;
                    document.getElementById("exp").value = 21;
                    document.getElementById("m").value = 4;
                    document.getElementById("ws").value = 3;
                    document.getElementById("bs").value = 3;
                    document.getElementById("s").value = 3;
                    document.getElementById("t").value = 3;
                    document.getElementById("w").value = 1;
                    document.getElementById("i").value = 3;
                    document.getElementById("a").value = 1;
                    document.getElementById("ld").value = 7;
                } else if (gangerType === "H") {
                    document.getElementById("cost").value = 60;
                    document.getElementById("exp").value = 61;
                    document.getElementById("m").value = 4;
                    document.getElementById("ws").value = 3;
                    document.getElementById("bs").value = 3;
                    document.getElementById("s").value = 3;
                    document.getElementById("t").value = 3;
                    document.getElementById("w").value = 1;
                    document.getElementById("i").value = 3;
                    document.getElementById("a").value = 1;
                    document.getElementById("ld").value = 7;
                } else if (gangerType === "L") {
                    document.getElementById("cost").value = 120;
                    document.getElementById("exp").value = 61;
                    document.getElementById("m").value = 4;
                    document.getElementById("ws").value = 4;
                    document.getElementById("bs").value = 4;
                    document.getElementById("s").value = 3;
                    document.getElementById("t").value = 3;
                    document.getElementById("w").value = 1;
                    document.getElementById("i").value = 4;
                    document.getElementById("a").value = 1;
                    document.getElementById("ld").value = 8;
                }
            }
        </script>
    </body>
</html>
