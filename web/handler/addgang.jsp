<%-- 
    Document   : addgang
    Created on : 8-Apr-2015, 10:55:20 AM
    Author     : weirdvector
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%

    Gang gang = new Gang();
    gang.setGangName(request.getParameter("gangName"));
    gang.setGangHouse(request.getParameter("house"));
    gang.setCredits(1000);
    
    int id = gangBean.addGang(gang);
    
    response.sendRedirect("../index.jsp?id=" + id);
    
%>
