<%-- 
    Document   : removeweapon
    Created on : 2-Apr-2015, 11:47:31 AM
    Author     : weirdvector
--%>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    
    //remove the weapon from the ganger's equipment
    int gangerId = Integer.parseInt(request.getParameter("id"));
    int weaponId = Integer.parseInt(request.getParameter("wepId"));
    int gangId = Integer.parseInt(request.getParameter("gangId"));
    int cost = Integer.parseInt(request.getParameter("cost"));
    gangBean.removeGangerEquipment(weaponId, gangerId);
    
    //add the weapon back into the gang's stash
    gangBean.addWeaponToStash(gangId, weaponId, cost);
    
    //forward back to updateganger.jsp
    response.sendRedirect("../updateganger.jsp?id=" + gangerId);
%>