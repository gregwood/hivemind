<%-- 
    Document   : index
    Created on : 27-Mar-2015, 9:41:03 PM
    Author     : weirdvector
--%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    int id;
    if (request.getParameter("id") != null) {
        id = Integer.parseInt(request.getParameter("id"));
    } else {
        id = 1;
    }
    Gang gang = gangBean.getGang(id);
    ArrayList<Gang> ganglist = gangBean.getGangs();
    
    //set the gang as an attribute so it can be used on the next page
    session.setAttribute("gang", gang);
    session.setAttribute("gangId", gang.getGangId());
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hivemind</title>
        <link rel='stylesheet' type='text/css' href='css/hivemind.css'>
        <link rel="shortcut icon" href="img/icon.png" />
    </head>
    <body>
        <div id="container">

            <h1>Hivemind</h1>

            <span>
                <select id="switchGang" onchange="switchGang()">
                    <option>Change gang...</option>
                    <% for (int i = 0; i < ganglist.size(); i++) { %>
                        <option value="<%= ganglist.get(i).getGangId() %>"><%= ganglist.get(i).getGangName()%></option>
                    <% } %>
                    <option>New gang...</option>
                </select>
            </span>

            <form action="postgame.jsp" method="post">
                <table id="gangerTable">
                    <tr>
                        <th class='stats'></th>
                        <th>Name</th>
                        <th class='stats'>M</th>
                        <th class='stats'>WS</th>
                        <th class='stats'>BS</th>
                        <th class='stats'>S</th>
                        <th class='stats'>T</th>
                        <th class='stats'>W</th>
                        <th class='stats'>I</th>
                        <th class='stats'>A</th>
                        <th class='stats'>Ld</th>
                        <th>Equipment</th>
                        <th>Skills</th>
                        <th>Injuries</th>
                        <th>Cost</th>
                        <th>Exp</th>
                        <th>Fleshwound</th>
                        <th>Kills</th>
                        <th>Objectives</th>
                        <th>Down</th>
                        <th>Out of Action</th>
                    </tr>
                    <%
                        while (gang.hasMoreGangers()) {
                            Ganger g = gang.nextGanger();
                    %>
                    <tr id="gangRow<%= g.getGangerId() %>">
                        <td><input type="checkbox" id="active<%= g.getGangerId() %>" name="active<%= g.getGangerId() %>" value="isActive" onchange="active(<%=g.getGangerId()%>)" checked></td>
                        <td><%= g.getName()%>, <%= g.getGangerType() %></td>
                        <td class='stats'><%= g.getMove()%></td>
                        <td class='stats' id="ganger_<%= g.getGangerId()%>_ws"><%= g.getWs()%></td>
                        <td class='stats' id="ganger_<%= g.getGangerId()%>_bs"><%= g.getBs()%></td>
                        <td class='stats'><%= g.getStrength()%></td>
                        <td class='stats'><%= g.getToughness()%></td>
                        <td class='stats'><%= g.getWounds()%></td>
                        <td class='stats'><%= g.getInitiative()%></td>
                        <td class='stats'><%= g.getAttack()%></td>
                        <td class='stats'><%= g.getLeadership()%></td>
                        <td class="textStats"><% 
                            ArrayList<Weapon> equipment = gangBean.getGangerEquipment(g.getGangerId());
                                for (Weapon weapon : equipment) {
                                    out.print(weapon.getWeaponName() + ", ");
                                }
                            %>
                        </td>
                        <td class="textStats"><%= gangBean.skillsList(g.getGangerId()) %></td>
                        <td class="textStats"><%= gangBean.injuriesList(g.getGangerId()) %></td>
                        <td><%= g.getCost()%></td>
                        <td><%= g.getExp()%></td>
                        <td><input type="checkbox" id="chkFleshwound<%= g.getGangerId()%>" onclick="fleshwound(<%= g.getGangerId()%>)"/></td>
                        <td class="radioCell">
                            <input type="checkbox" name="kills<%= g.getGangerId()%>" value="1"/>
                            <input type="checkbox" name="kills<%= g.getGangerId()%>" value="2"/>
                            <input type="checkbox" name="kills<%= g.getGangerId()%>" value="3"/>
                        </td>
                        <td class="radioCell">
                            <input type="checkbox" name="obj<%= g.getGangerId()%>" value="1"/>
                            <input type="checkbox" name="obj<%= g.getGangerId()%>" value="2"/>
                            <input type="checkbox" name="obj<%= g.getGangerId()%>" value="3"/>
                        </td>
                        <td><input type="checkbox" name="down<%= g.getGangerId()%>"/></td>
                        <td>
                            <input type="checkbox" name="outOfAction<%= g.getGangerId()%>"/>
                            <a href="updateganger.jsp?id=<%= g.getGangerId() %>"><img class='wrench' src="img/wrench.png" /></a>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>

                <div id="secondPanel">

                    <table id="gangInfo">
                        <caption>Gang Info</caption>
                        <tr>
                            <th>Gang name:</th>
                            <td><%= gang.getGangName()%></td>
                        </tr>
                        <tr>
                            <th>House:</th>
                            <td><%= gang.getGangHouse()%></td>
                        </tr>
                        <tr>
                            <th>Gang Rating:</th>
                            <td><%= gang.getGangRating()%></td>
                        </tr>
                        <tr>
                            <th>Credits:</th>
                            <td><%= gang.getCredits()%></td>
                        </tr>
                        <tr>
                            <th>Territory:</th>
                            <th>Income: </th>
                        </tr>
                        <%
                            //display the territory list for the gang
                            ArrayList<Territory> territories = gangBean.getTerritories(gang.getGangId());
                            for (Territory t : territories) {
                                out.print("<tr>");
                                out.print("<td>" + t.getTerritoryName() + "</td>");
                                out.print("<td>" + t.getIncome() + "</td>");
                                out.print("</tr>");
                            }
                        %>
                        <tr>
                            <td class="buttonCell"><a href="addganger.jsp?id=<%= gang.getGangId() %>"><input type="button" value="Recruit"></a></td>
                            <td class="buttonCell"><a href='store.jsp?id=<%= gang.getGangId() %>'><input type="button" value="Shop"></a></td>
                        </tr>
                    </table>


                    <table id="weaponReference">
                        <caption>Weapon Reference</caption>
                        <tr>
                            <th></th>
                            <th colspan="2">Range</th>
                            <th colspan="2">To Hit</th>
                            <th colspan="2"></th>
                            <th rowspan="2">Save<br>Mod.</th>
                            <th rowspan="2">Ammo<br>Roll</th>
                        </tr>    
                        <tr>
                            <th class="gunName">Gun</th>
                            <th>Short</th>
                            <th>Long</th>
                            <th>Short</th>
                            <th>Long</th>
                            <th>Strength</th>
                            <th>Damage</th>
                        </tr>
                        <%
                            //display all of the information for distinct weapons carried by the gangers.
                            ArrayList<Weapon> weapons = gangBean.getGangEquipment(gang.getGangId());
                            for (Weapon w : weapons) {
                                out.print("<tr>");
                                out.print("<td class='weaponStats'>" + w.getWeaponName() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getShortRange()+ "</td>");
                                out.print("<td class='weaponStats'>" + w.getLongRange()+ "</td>");
                                out.print("<td class='weaponStats'>" + w.getHitShort() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getHitLong()+ "</td>");
                                out.print("<td class='weaponStats'>" + w.getStrength() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getDamage() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getSaveMod() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getAmmoRoll() + "</td>");
                                out.print("</tr>");
                            }
                        %>
                    </table>
                    <table id="postGame">
                        <caption>Post Game</caption>
                        <tr>
                            <td>Game Type:</td>
                            <td>
                                <select name="gameType">
                                    <option value='gangfight'>Gangfight</option>
                                    <option value='scavengers'>Scavengers</option>
                                    <option value='hit and run'>Hit and Run</option>
                                    <option value='ambush'>Ambush</option>
                                    <option value='the raid'>The Raid</option>
                                    <option value='rescue'>Rescue</option>
                                    <option value='shootout'>Shootout</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Win: <input type='radio' name='winLose' value='win' required/></td>
                            <td>Lose: <input type='radio' name='winLose' value='lose' required/></td>
                        </tr>
                        <tr>
                            <td>Attacker: <input type='radio' name='attackDefend' value='attack' required/></td>
                            <td>Defender: <input type='radio' name='attackDefend' value='defend' required/></td>
                        </tr>
                        <tr>
                            <td>Opponent Gang Rating: </td>
                            <td><input type='number' name='oppGangRating' required></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="buttonCell">
                                <input id="submitButton" type="submit" value="Post Game">
                            </td>
                        </tr>
                    </table>
                </div>

            </form>
        </div>

        <script>
            function fleshwound(n) {
                var chk = "chkFleshwound" + n;
                var ws = "ganger_" + n + "_ws";
                var bs = "ganger_" + n + "_bs";
                if (document.getElementById(chk).checked) {
                    document.getElementById(ws).innerHTML = parseInt(document.getElementById(ws).innerHTML) - 1;
                    document.getElementById(ws).style.background = "yellow";
                    document.getElementById(bs).innerHTML = parseInt(document.getElementById(bs).innerHTML) - 1;
                    document.getElementById(bs).style.background = "yellow";
                } else {
                    document.getElementById(ws).innerHTML = parseInt(document.getElementById(ws).innerHTML) + 1;
                    document.getElementById(ws).style.background = "#f5f5f5";
                    document.getElementById(bs).innerHTML = parseInt(document.getElementById(bs).innerHTML) + 1;
                    document.getElementById(bs).style.background = "#f5f5f5";
                }
            }
            
            function switchGang() {
                var n = document.getElementById("switchGang").value;
                if (n === "New gang...") {
                    window.location.replace("newgang.jsp");
                } else {
                    var url = "index.jsp?id=" + n;
                    window.location.replace(url);
                }
            }
            
            function active(id) {
                //if the active checkbox is not selected, grey out those cells.
                if (document.getElementById("active" + id).checked) {
                    var tds = document.getElementById("gangRow" + id).children;
                    for (i = 0; i < tds.length; i++) {
                        tds[i].style.backgroundColor = "white";
                    }
                } else {
                    var tds = document.getElementById("gangRow" + id).children;
                    for (i = 0; i < tds.length; i++) {
                        tds[i].style.backgroundColor = "#878787";
                    }
                }
            }
        </script>
    </body>
</html>
