<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hivemind</title>
        <link rel='stylesheet' type='text/css' href='css/hivemind.css'>
        <link rel="shortcut icon" href="img/icon.png" />
    </head>
    <body>
        <div id="container">

            <h1>Hivemind</h1>

            <form action="handler/addgang.jsp" method="get">
                <table class="center">
                    <tr><td>Name:</td><td><input type="text" name="gangName" required></td></tr>
                    <tr><td>House:</td><td>
                            <select name="house">
                                <option value="cawdor">Cawdor</option>
                                <option value="escher">Escher</option>
                                <option value="delaque">Delaque</option>
                                <option value="goliath">Goliath</option>
                                <option value="orlock">Orlock</option>
                                <option value="vansaar">Van Saar</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class='buttonCell'>
                            <a href="index.jsp"><input type="button" value="Cancel"></a>
                        </td>
                        <td class='buttonCell'>
                            <input type="submit" value="Create gang">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
