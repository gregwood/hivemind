<%-- 
    Document   : skills
    Created on : 29-Mar-2015, 12:21:48 PM
    Author     : greg
--%>

<%@page import="src.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    //get gang from session
    Gang gang = (Gang) session.getAttribute("gang");
    Skills skills = new Skills();

    //make the new Gang to put the updated Gangers into
    Gang updatedGang = new Gang();
    updatedGang.setGangName(gang.getGangName());
    updatedGang.setGangHouse(gang.getGangHouse());
    updatedGang.setGangId((Integer)session.getAttribute("gangId"));
    updatedGang.setCredits(gang.getCredits());

    for (Ganger g : gang.getGangArrayList()) {
        
        if (request.getParameter("pickGangSkill" + g.getGangerId()) != null) {
            String choice = request.getParameter("pickGangSkill" + g.getGangerId());
            if (choice.equals("agility")) {
                g = skills.randomAgilitySkill(g);
            } else if (choice.equals("combat")) {
                g = skills.randomCombatSkill(g);
            } else if (choice.equals("ferocity")) {
                g = skills.randomFerocitySkill(g);
            } else if (choice.equals("muscle")) {
                g = skills.randomMuscleSkill(g);
            } else if (choice.equals("shooting")) {
                g = skills.randomShootingSkill(g);
            } else if (choice.equals("stealth")) {
                g = skills.randomStealthSkill(g);
            } else if (choice.equals("techno")) {
                g = skills.randomTechnoSkill(g);
            }
        }
        if (request.getParameter("pickAnySkill" + g.getGangerId()) != null) {
            String choice = request.getParameter("pickAnySkill" + g.getGangerId());
            if (choice.equals("agility")) {
                g = skills.randomAgilitySkill(g);
            } else if (choice.equals("combat")) {
                g = skills.randomCombatSkill(g);
            } else if (choice.equals("ferocity")) {
                g = skills.randomFerocitySkill(g);
            } else if (choice.equals("muscle")) {
                g = skills.randomMuscleSkill(g);
            } else if (choice.equals("shooting")) {
                g = skills.randomShootingSkill(g);
            } else if (choice.equals("stealth")) {
                g = skills.randomStealthSkill(g);
            } else if (choice.equals("techno")) {
                g = skills.randomTechnoSkill(g);
            }
        }
        
        updatedGang.addGanger(g);
    }

    //set the gang as an attribute so it can be used on the next page
    session.setAttribute("gang", updatedGang);
    
    //update the gang on the database
    GangBean gangBean = new GangBean();
    gangBean.writeSheet(updatedGang);
    
    //redirect back to the index page
    response.sendRedirect("index.jsp?id=" + updatedGang.getGangId());
%>