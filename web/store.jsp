<%--    
    Document   : store
    Created on : 1-Apr-2015
    Author     : weirdvector
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="src.*" %>
<jsp:useBean id="gangBean" class="src.GangBean" scope="session"></jsp:useBean>
<%
    session.setAttribute("gangId", request.getParameter("id"));
    String id = request.getParameter("id");
    Gang gang = gangBean.getGang(Integer.parseInt(id));
    ArrayList<Gang> ganglist = gangBean.getGangs();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hivemind</title>
        <link rel="stylesheet" type="text/css" href="css/hivemind.css">
        <link rel="shortcut icon" href="img/icon.png" />
    </head>
    <body>
        <div id="container">
            <h1>Store</h1>
            <div id="editPanel">
                <form action="handler/buy.jsp" method="post">
                    <table id="store">
                        <tr>
                            <th></th>
                            <th colspan="2">Range</th>
                            <th colspan="2">To Hit</th>
                            <th colspan="2"></th>
                            <th rowspan="2">Save<br>Mod.</th>
                            <th rowspan="2">Ammo<br>Roll</th>
                        </tr>    
                        <tr>
                            <th class="gunName">Gun</th>
                            <th>Short</th>
                            <th>Long</th>
                            <th>Short</th>
                            <th>Long</th>
                            <th>Strength</th>
                            <th>Damage</th>
                            <th>Type</th>
                            <th>Cost</th>
                            <th>Number</th>
                            <th>Subtotal</th>
                        </tr>
                        <tr>
                        <%
                            //show the Rare items in stock
                            RareItems ri = new RareItems();
                            ArrayList<Weapon> rares = ri.getRareItems();
                            for (Weapon w : rares) {
                                out.print("<tr>");
                                out.print("<td class='weaponStats rare'>" + w.getWeaponName() + "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getShortRange()+ "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getLongRange()+ "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getHitShort() + "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getHitLong()+ "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getStrength() + "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getDamage() + "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getSaveMod() + "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getAmmoRoll() + "</td>");
                                out.print("<td class='weaponStats rare'>" + w.getType() + "</td>");
                                out.print("<td id='wepCost" + w.getWeaponId() +"' class='rare'>" + w.getCost() + "</td>");
                                out.print("<input type='hidden' name='costActual" + w.getWeaponId() + "' value='" + w.getCost() + "'>");
                                out.print("<td class='rare'><select id='amount" + w.getWeaponId() +"' name='amount" + w.getWeaponId() +"' onchange='updateDisplay(" + w.getWeaponId() + ")'>");
                                for (int i = 0; i <= 1; i++) {
                                    out.println("<option value='" + i + "'>" + i + "</option>");
                                }
                                out.print("</td>");
                                out.print("<td class='subtotal rare' id='subTotalWep" + w.getWeaponId() + "'>0</td>");
                                out.print("</tr>");
                            }
                        %>
                        </tr>
                        <tr>
                            <th></th>
                            <th colspan="2">Range</th>
                            <th colspan="2">To Hit</th>
                            <th colspan="2"></th>
                            <th rowspan="2">Save<br>Mod.</th>
                            <th rowspan="2">Ammo<br>Roll</th>
                        </tr>    
                        <tr>
                            <th class="gunName">Gun</th>
                            <th>Short</th>
                            <th>Long</th>
                            <th>Short</th>
                            <th>Long</th>
                            <th>Strength</th>
                            <th>Damage</th>
                            <th>Type</th>
                            <th>Cost</th>
                            <th>Number</th>
                            <th>Subtotal</th>
                        </tr>
                        <tr>
                        <%
                            //display all of the information for distinct weapons carried by the gangers.
                            ArrayList<Weapon> weapons = gangBean.getCommonEquipment();
                            for (Weapon w : weapons) {
                                out.print("<tr>");
                                out.print("<td class='weaponStats'>" + w.getWeaponName() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getShortRange()+ "</td>");
                                out.print("<td class='weaponStats'>" + w.getLongRange()+ "</td>");
                                out.print("<td class='weaponStats'>" + w.getHitShort() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getHitLong()+ "</td>");
                                out.print("<td class='weaponStats'>" + w.getStrength() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getDamage() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getSaveMod() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getAmmoRoll() + "</td>");
                                out.print("<td class='weaponStats'>" + w.getType() + "</td>");
                                out.print("<td id='wepCost" + w.getWeaponId() +"'>" + w.getCost() + "</td>");
                                out.print("<input type='hidden' name='costActual" + w.getWeaponId() + "' value='" + w.getCost() + "'>");
                                out.print("<td><select id='amount" + w.getWeaponId() +"' name='amount" + w.getWeaponId() +"' onchange='updateDisplay(" + w.getWeaponId() + ")'>");
                                for (int i = 0; i <= 10; i++) {
                                    out.println("<option value='" + i + "'>" + i + "</option>");
                                }
                                out.print("</td>");
                                out.print("<td class='subtotal' id='subTotalWep" + w.getWeaponId() + "'>0</td>");
                                out.print("</tr>");
                            }
                        %>
                        </tr>
                        <tr>
                            <td colspan='9'></td>
                            <td colspan='3'><strong>Total</strong></td>
                            <td><input type='text' class='subtotal' id="totalDisplay" name="total" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td colspan='9'></td>
                            <td colspan='4' class='buttonCell'><input id="submit" type='submit' value='Checkout'></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>

        <script>
            function updateDisplay(id) {
                var cost = parseInt(document.getElementById("wepCost" + id).innerHTML);
                var amount = parseInt(document.getElementById("amount" + id).value);
                document.getElementById("subTotalWep" + id).innerHTML = (cost * amount);
                
                updateTotal();
            }
            
            function updateTotal() {
                var total = 0;
                //78 is the number of equipment items
                for (i = 0; i < 78; i++) {
                    var subtotal = "subTotalWep" + i;
                    if (document.getElementById(subtotal) !== null) {
                        total += parseInt(document.getElementById(subtotal).innerHTML);
                    }
                }
                document.getElementById("totalDisplay").value = total;
                
                if (total >  <%= gang.getCredits() %>) {
                    document.getElementById("totalDisplay").style.color = "Red";
                    document.getElementById("submit").disabled = "true";
                } else {
                    document.getElementById("totalDisplay").style.color = "Black";
                    document.getElementById("submit").enabled = "false";
                }
            }
        </script>
    </body>
</html>
